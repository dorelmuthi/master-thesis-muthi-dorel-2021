from tensorflow.keras.datasets import cifar10, mnist, fashion_mnist
from tensorflow.keras.layers import Input, Dense, Reshape, Flatten, Dropout
from tensorflow.keras.layers import BatchNormalization, Activation, ZeroPadding2D
from tensorflow.keras.layers import LeakyReLU
from tensorflow.keras.layers import UpSampling2D, Conv2D
from tensorflow.keras.models import Sequential, Model
from tensorflow.keras.optimizers import Adam
import tensorflow as tf
from tensorflow import keras

import matplotlib.pyplot as plt
import numpy as np
import csv
import time

from utils import *

#from abc import ABC, abstractmethod

"""
class MyModel(ABC):
	def __init__(self):
		pass
	@abstractmethod
	def train(self):
		pass
"""

class DCGAN(keras.Model):
	def __init__(self, **kwargs):
		super(DCGAN, self).__init__(name="DCGAN",**kwargs)
		self.trainingGeneratedImagesPath = None
		self.resultsPath = None
		self.resultsCsvFilename = None
		self.modelsPath = None
		self.imgsNb = None
		self.width = None
		self.height = None
		self.channels = None
		self.img_shape = None
		self.latent_dim = None
		self.optimizerG = None
		self.optimizerD = None
		self.cross_entropy = None
		self.discriminator = None
		self.generator = None
		self.batch_size = 32
		self.d_loss_tracker = keras.metrics.Mean(name="d_loss")
		self.g_loss_tracker = keras.metrics.Mean(name="g_loss")
		self.d_accuracy_real_tracker = keras.metrics.Mean(name="d_accuracy_real")
		self.d_accuracy_fake_tracker = keras.metrics.Mean(name="d_accuracy_fake")
		self.d_accuracy_tracker = keras.metrics.Mean(name="d_accuracy")

	def compile(self, imgs_array, dataset_name):
		super(DCGAN, self).compile()
		fromLogits = False
		self.trainingGeneratedImagesPath = "./Training_Generated_Images/"+dataset_name+"/"+self.name+"/"
		self.resultsPath = "./Results/"+dataset_name+"/"
		self.resultsCsvFilename = "results_"+self.name+".csv"
		self.modelsPath = "./Models/"+dataset_name+"/"
		self.initialize_directories()
		self.imgsNb, self.width, self.height, self.channels = imgs_array.shape
		self.img_shape = (self.width, self.height, self.channels)
		self.latent_dim = 100
		self.optimizerG = Adam(learning_rate=0.0002, beta_1=0.5)
		self.optimizerD = Adam(learning_rate=0.0002, beta_1=0.5)
		self.cross_entropy = tf.keras.losses.BinaryCrossentropy(from_logits=fromLogits)
		self.discriminator = self.build_discriminator()
		self.generator = self.build_generator()

	def getGeneratorLoss(self, fake_output):
		return self.cross_entropy(tf.ones_like(fake_output), fake_output)

	def getDiscriminatorLoss(self, real_output, fake_output):
		real_loss = self.cross_entropy(tf.ones_like(real_output), real_output)
		fake_loss = self.cross_entropy(tf.zeros_like(fake_output), fake_output)
		total_loss = real_loss + fake_loss
		return real_loss, fake_loss, total_loss

	@tf.function
	def train_step(self, images):
		# images is a batch/a sub-dataset
		noise = tf.random.normal([self.batch_size, self.latent_dim])
		with tf.GradientTape() as gen_tape, tf.GradientTape() as disc_tape:
			generated_images = self.generator(noise, training=True)
			real_output = self.discriminator(images, training=True)
			fake_output = self.discriminator(generated_images, training=True)
			g_loss = self.getGeneratorLoss(fake_output)
			d_loss_real, d_loss_fake, d_loss = self.getDiscriminatorLoss(real_output, fake_output)
			d_loss = tf.reduce_mean(d_loss)
			g_loss = tf.reduce_mean(g_loss)
			d_accuracy_real = tf.math.reduce_sum(real_output)/tf.cast(len(real_output), tf.float32)
			d_accuracy_fake = tf.math.reduce_sum(fake_output)/tf.cast(len(fake_output), tf.float32)
			d_accuracy = tf.math.reduce_mean([d_accuracy_real, d_accuracy_fake])
		gradients_of_generator = gen_tape.gradient(g_loss, self.generator.trainable_variables)
		gradients_of_discriminator = disc_tape.gradient(d_loss, self.discriminator.trainable_variables)
		self.optimizerG.apply_gradients(zip(gradients_of_generator, self.generator.trainable_variables))
		self.optimizerD.apply_gradients(zip(gradients_of_discriminator, self.discriminator.trainable_variables))
		self.d_loss_tracker.update_state(d_loss)
		self.g_loss_tracker.update_state(g_loss)
		self.d_accuracy_real_tracker.update_state(d_accuracy_real)
		self.d_accuracy_fake_tracker.update_state(d_accuracy_fake)
		self.d_accuracy_tracker.update_state(d_accuracy)
		return {"d_loss": self.d_loss_tracker.result(), "g_loss": self.g_loss_tracker.result(), 
			"d_accuracy_real": self.d_accuracy_real_tracker.result(), "d_accuracy_fake": self.d_accuracy_fake_tracker.result(),
			"d_accuracy": self.d_accuracy_tracker.result()}

	def build_generator(self):
		model = Sequential(name="Generator")
		model.add(Dense(128 * 7 * 7, activation="relu", input_dim=self.latent_dim))
		model.add(Reshape((7, 7, 128)))
		model.add(UpSampling2D())
		model.add(Conv2D(128, kernel_size=3, padding="same"))
		model.add(BatchNormalization(momentum=0.8))
		model.add(Activation("relu"))
		model.add(UpSampling2D())
		model.add(Conv2D(64, kernel_size=3, padding="same"))
		model.add(BatchNormalization(momentum=0.8))
		model.add(Activation("relu"))
		model.add(Conv2D(self.channels, kernel_size=3, padding="same"))
		model.add(Activation("tanh"))
		model.summary()
		return model

	def build_discriminator(self):
		model = Sequential(name="Discriminator")
		model.add(Conv2D(32, kernel_size=3, strides=2, input_shape=self.img_shape, padding="same"))
		model.add(LeakyReLU(alpha=0.2))
		model.add(Dropout(0.25))
		model.add(Conv2D(64, kernel_size=3, strides=2, padding="same"))
		model.add(ZeroPadding2D(padding=((0,1),(0,1))))
		model.add(BatchNormalization(momentum=0.8))
		model.add(LeakyReLU(alpha=0.2))
		model.add(Dropout(0.25))
		model.add(Conv2D(128, kernel_size=3, strides=2, padding="same"))
		model.add(BatchNormalization(momentum=0.8))
		model.add(LeakyReLU(alpha=0.2))
		model.add(Dropout(0.25))
		model.add(Conv2D(256, kernel_size=3, strides=1, padding="same"))
		model.add(BatchNormalization(momentum=0.8))
		model.add(LeakyReLU(alpha=0.2))
		model.add(Dropout(0.25))
		model.add(Flatten())
		model.add(Dense(1, activation='sigmoid'))
		model.summary()
		return model

	def save_results(self, epoch, resultsList, sample_interval):
		loss_interval = 1
		if epoch % loss_interval == 0:
			with open(self.resultsPath + self.resultsCsvFilename, "a", newline='') as csvfile:
				writer = csv.writer(csvfile, delimiter=';')
				writer.writerow([epoch]+resultsList)
		if epoch % sample_interval == 0: # Save generated image samples
			self.sample_images(epoch)

	def sample_images(self, epoch, n=10):
		noise = np.random.normal(0, 1, (n, self.latent_dim))
		gen_imgs = self.generator.predict(noise)
		# Rescale images 0 - 1
		#gen_imgs = 0.5 * gen_imgs + 0.5
		fig, axs = plt.subplots(nrows=1, ncols=n)
		cnt = 0
		for j in range(n):
			imgs = gen_imgs[cnt] = (gen_imgs[cnt]) * 127.5 + 127.5
			imgs = imgs.astype(np.uint8)
			if self.channels == 3:
				axs[j].imshow(imgs)
			elif self.channels == 1:
				axs[j].imshow(imgs, cmap='gray')
			axs[j].axis('off')
			cnt += 1
		axs[0].set_title("Epoch " + str(epoch))
		fig.savefig(self.trainingGeneratedImagesPath+"%d.png" % epoch, bbox_inches='tight')
		plt.close()

	def generate_imgs(self, imgsNb=10):
		noise = np.random.normal(0, 1, (imgsNb, self.latent_dim))
		gen_imgs = self.generator.predict(noise)
		gen_imgs = (gen_imgs) * 127.5 + 127.5
		gen_imgs = gen_imgs.astype(np.uint8)
		gen_imgs = np.squeeze(gen_imgs)
		return gen_imgs

	def generate_imgs_to(self, path, imgsNb=10):
		genImgs = self.generate_imgs(imgsNb)
		if self.channels == 1:
			genImgsPIL = convertToImgPIL(genImgs, colorMode="gray")
		elif self.channels == 3:
			genImgsPIL = convertToImgPIL(genImgs, colorMode="color")
		genImgsPIL = convertToImgPIL(genImgs)
		emptyDirectory(path)
		createDirectory(path)
		saveImgsPIL(genImgsPIL, path)
		#self.generate_gif(genImgsPIL, path)

	def generate_gif(self, imgsPIL, path):
		generateGif(imgsPIL, path, filename=self.name)

	def initialize_directories(self):
		createDirectory(self.trainingGeneratedImagesPath)
		createDirectory(self.resultsPath)
		removeFile(self.resultsPath + self.resultsCsvFilename)
		emptyDirectory(self.trainingGeneratedImagesPath)

	def save_generator(self):
		createDirectory(self.modelsPath)
		self.generator.save(self.modelsPath+self.name+"_generator.h5")

def trainDCGAN(imgs_array, dataset_name, epochs=10, batch_size=32):
	np.random.seed(1)
	tf.random.set_seed(1)
	print(imgs_array.shape)
	f = open("training_time.txt", "a")
	start = time.time()
	dcgan = DCGAN()
	dcgan.compile(imgs_array, dataset_name)
	generativeModelName = "DCGAN"
	#batches = tf.data.Dataset.from_tensor_slices(imgs_array).shuffle(len(imgs_array)).batch(batch_size, drop_remainder=True)
	#batches = batches.take(100)
	history = dcgan.fit(imgs_array, epochs=epochs, batch_size=32, verbose=2)
	csvfile = open(dcgan.resultsPath + dcgan.resultsCsvFilename, 'w', newline='')
	writer = csv.writer(csvfile, delimiter=';')
	print(history.history.keys())
	for epoch in range(len(history.history['d_loss'])):
		d_loss = history.history['d_loss'][epoch]
		g_loss = history.history['g_loss'][epoch]
		d_accuracy_real = history.history['d_accuracy_real'][epoch]
		d_accuracy_fake = history.history['d_accuracy_fake'][epoch]
		d_accuracy = history.history['d_accuracy'][epoch]
		print("epoch", epoch+1, "d_loss", d_loss, "g_loss", g_loss, 
			"d_accuracy_real", d_accuracy_real, "d_accuracy_fake", d_accuracy_fake, "d_accuracy", d_accuracy)
		resultsList = [d_loss, 100*d_accuracy, 100*d_accuracy_real, 100*d_accuracy_fake, g_loss]
		writer.writerow([epoch+1]+resultsList)
	csvfile.close()
	dcgan.save_generator()
	end = time.time()
	elapsedTime_secs = end - start
	elapsedTime_hms = convert_secs_to_hms(elapsedTime_secs)
	print("Elapsed time:", elapsedTime_hms)
	f.write(generativeModelName+", "+dataset_name+", Epochs "+str(epochs)+", Batch Size "+str(batch_size)+", Training Time "+elapsedTime_hms+"\n")
	f.close()
	generatedImagesPath = "./Generated_Images/"+dataset_name+"/"+generativeModelName+"/"
	dcgan.generate_imgs_to(generatedImagesPath, 100)

if __name__ == '__main__':
	"""
	imgs_array = load_tf_dataset()
	imgs_array = load_dataset(path="./bob_ross_dataset_28x28/")
	imgs_array = load_dataset(path="./pokemons_dataset_28x28/")
	imgs_array = load_dataset(path="./anime_faces_dataset_28x28/")
	imgs_array = load_dataset(path="./cat_faces_dataset_28x28/")
	imgs_array = load_dataset(path="./human_faces_dataset_28x28/")
	imgs_array = load_dataset(path="./abstract_paintings_dataset_28x28/")
	imgs_array = load_dataset(path="./flowers_dataset_28x28/")
	imgs_array = load_dataset(path="./sunflowers_dataset_28x28/")
	imgs_array = load_dataset(path="./celebrities_faces_dataset_28x28/")
	imgs_array = load_dataset(path="./simpsons_faces_dataset_28x28/")
	imgs_array = load_dataset(path="./triangles_dataset_28x28/")
	imgs_array = load_dataset(path="./shapes_dataset_28x28/")
	imgs_array = load_dataset(path="./birds_dataset_28x28/")
	imgs_array = load_dataset(path="./animal_faces_dataset_28x28/")
	imgs_array = load_tf_dataset(classNbs=[0])
	"""
	#imgs_array = load_tf_dataset(datasetNb=2, colorMode="color")

	"""
	datasetsPath = "./Datasets/"
	dataset_name = "anime_faces_dataset_28x28"
	imgs_array = load_dataset(path=datasetsPath+dataset_name+"/")
	trainDCGAN(imgs_array, dataset_name, epochs=10, batch_size=32)
	"""

	imgs_array = load_tf_dataset(datasetNb=2, colorMode="gray")
	dataset_name = "Fashion_MNIST"
	trainDCGAN(imgs_array, dataset_name, epochs=2, batch_size=32)

	"""
	datasetsPath = "./Datasets/"
	dataset_name = "human_faces_dataset_28x28"
	imgs_array = load_dataset(path=datasetsPath+dataset_name+"/")
	trainDCGAN(imgs_array, dataset_name, epochs=1000, batch_size=32)
	"""