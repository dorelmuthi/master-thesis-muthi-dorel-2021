import os
import glob
import PIL
import PIL.Image
import numpy as np
from tensorflow.keras.datasets import cifar10, mnist, fashion_mnist

# Functions for files and folder
# ------------------------------
def createDirectory(path):
	if not os.path.isdir(path):
		os.makedirs(path)

def removeFile(path):
	if os.path.isfile(path):
		os.remove(path)

def emptyDirectory(path):
	if os.path.isdir(path):
		files = glob.glob(path+"*")
		for file in files:
			os.remove(file)
#-------------------------------

# Functions for images
# ------------------------------
def convertColorMode(imgPIL, colorMode):
	if colorMode == "color":
		imgPIL = imgPIL.convert('RGB')
	elif colorMode == "gray":
		imgPIL = imgPIL.convert('L')
	return imgPIL

def resizeImgsArray(imgsArray, width, height, colorMode="color"):
	tmp = []
	for imgArray in imgsArray:
		imgPIL = PIL.Image.fromarray(imgArray)
		imgPIL = imgPIL.resize((width, height))
		imgPIL = convertColorMode(imgPIL, colorMode)
		array = np.asarray(imgPIL)
		tmp.append(array)
		imgPIL.close()
	imgsArray = np.asarray(tmp)
	return imgsArray

# Used to change the color mode
def convertImgsArray(imgsArray ,colorMode):
	tmp = []
	for imgArray in imgsArray:
		imgPIL = PIL.Image.fromarray(imgArray)
		imgPIL = convertColorMode(imgPIL, colorMode)
		array = np.asarray(imgPIL)
		tmp.append(array)
	imgsArray = np.asarray(tmp)
	return imgsArray

def convertToImgPIL(imgsArray, colorMode="color"):
	imgsPIL = []
	for imgArray in imgsArray:
		imgPIL = PIL.Image.fromarray(imgArray)
		imgPIL = convertColorMode(imgPIL, colorMode)
		imgsPIL.append(imgPIL)
	return imgsPIL

def convertToImgsArray(imgsPIL):
	imgsArray = []
	for imgPIL in imgsPIL:
		imgArray = np.asarray(imgPIL)
		imgsArray.append(imgArray)
		imgPIL.close()
	imgsArray = np.asarray(imgsArray)
	return imgsArray

def saveImgsPIL(imgsPIL, path, colorMode="color", imgFormat="png"):
	for idx, imgPIL in enumerate(imgsPIL):
		imgPIL = convertColorMode(imgPIL, colorMode)
		imgPIL.save(path+str(idx+1)+"."+ imgFormat)

def loadImgsPIL(path, colorMode="color",  imgFormat="png"):
	imgsPath = glob.glob(path+"*."+imgFormat)
	# Sort the paths by the number of the image.
	tmp = []
	for imgPath in imgsPath:
		imgNb = os.path.split(imgPath)[-1][:-4]
		tmp.append([int(imgNb), imgPath])
	tmp.sort(key = lambda x: x[0]) # sort by first element the lists of tmp
	imgsPath = [elem[1] for elem in tmp]
	imgsPIL = []
	for imgPath in imgsPath:
		imgPIL = PIL.Image.open(imgPath)
		imgsPIL.append(imgPIL)
	return imgsPIL

def saveImgsArray(path, imgsArray):
	for idx, imgArray in enumerate(imgsArray):
		imgPIL = PIL.Image.fromarray(imgArray)
		imgPIL.save(path+str(idx+1)+".png")
		imgPIL.close()

def loadImgsArray(path, colorMode="color", width=32, height=32, imgFormat="png", imgsNb=None):
	imgsPath = glob.glob(path+"*."+imgFormat)
	# Sort the paths by the number of the image.
	tmp = []
	for imgPath in imgsPath:
		imgNb = os.path.split(imgPath)[-1][:-4]
		tmp.append([int(imgNb), imgPath])
	tmp.sort(key = lambda x: x[0]) # sort by first element the lists of tmp
	imgsPath = [elem[1] for elem in tmp]
	imgsArray = []
	if imgsNb != None:
		imgsPath = imgsPath[:imgsNb]
	for imgPath in imgsPath:
		imgPIL = PIL.Image.open(imgPath)
		imgArray = convertToImgsArray([imgPIL])[0]
		imgArray = resizeImgsArray([imgArray], width, height)[0]
		imgsArray.append(imgArray)
		imgPIL.close()
	imgsArray = np.asarray(imgsArray)
	return imgsArray

# without sorting by number of the image
# add format as parameter
def loadImgsArray2(path, colorMode="color", width=32, height=32, imgFormat="png", imgsNb=None):
	imgsPath = glob.glob(path+"*."+imgFormat)
	if imgsNb != None:
		imgsPath = imgsPath[:imgsNb]
	imgsArray = []
	for imgPath in imgsPath:
		imgPIL = PIL.Image.open(imgPath)
		imgArray = convertToImgsArray([imgPIL])[0]
		imgArray = resizeImgsArray([imgArray], width, height)[0]
		imgsArray.append(imgArray)
		imgPIL.close()
	imgsArray = np.asarray(imgsArray)
	return imgsArray

# To load from folder containing images in folders
def getImagesPathOf(folderPath): # folder with subfolders
	folderElements = os.listdir(folderPath) # all files and directories
	directories = [elem for elem in folderElements if os.path.isdir(os.path.join(folderPath, elem))]
	imagesPath = []
	for directory in directories:
		path = os.path.join(folderPath, directory)
		pathElements = os.listdir(path) # all files and directories
		filesName = [os.path.join(path, elem) for elem in pathElements if os.path.isfile(os.path.join(path, elem))]
		imagesPath.extend(filesName)
	# add a verification of the image format, select only images.
	return imagesPath

# To load from folder containing images in folders
def loadImgsFromSubfoldersOf(datasetPath, colorMode = "color"):
	imagesPath = getImagesPathOf(datasetPath)
	#print(len(imagesPath))
	trainImgs = []
	for imagePath in imagesPath:
		imgArray = loadImgAsArray(imagePath, colorMode)
		trainImgs.append(imgArray)
	trainImgs = np.asarray(trainImgs)
	print("Train Imgs Shape", trainImgs.shape)
	return trainImgs # array

def loadImgAsArray(imagePath, colorMode = "color"):
	image = PIL.Image.open(imagePath)
	if colorMode == "gray": # convert image to grayscale
		image = image.convert('L')
	elif colorMode == "color":
		image = image.convert('RGB')
	imgArray = np.asarray(image)
	return imgArray

def generateGif(imgsPIL, path, filename):
	imgsPIL[0].save(path+filename+".gif", save_all=True, append_images=imgsPIL[1:], optimize=False, duration=150, loop=0)
#-------------------------------

# Functions to load dataset for training
#-------------------------------
def load_tf_dataset(datasetNb=2, colorMode="gray", width=None, height=None, classNbs = [], normalization=1):
	if datasetNb == 1:
		print("MNIST")
		(x_train, y_train), (x_test, y_test) = mnist.load_data()
	elif datasetNb == 2:
		print("Fashion MNIST")
		# Labels 0 T-shirt/top 1 Trouser 2 Pullover 3 Dress 4 Coat 5 Sandal 6 Shirt 7 Sneaker 8 Bag 9 Ankle boot
		(x_train, y_train), (x_test, y_test) = fashion_mnist.load_data()
	elif datasetNb == 3:
		print("CIFAR")
		# cifar_10 = ["airplane", "automobile", "bird", "cat", "deer", "dog", "frog", "horse", "ship", "truck"]
		(x_train, y_train), (x_test, y_test) = cifar10.load_data()
	if classNbs != []:
		x_train = [x_train[i] for i in range(len(y_train)) if y_train[i] in classNbs]
	x_train = convertImgsArray(x_train, colorMode=colorMode)
	if type(width) == int and type(height) == int:
		x_train = resizeImgsArray(x_train, width=width, height=height, colorMode=colorMode)
	if normalization == 1:
		x_train = (x_train.astype('float32') - 127.5) / 127.5 # Normalize the images to [-1, 1]
	elif normalization == 2:
		x_train = (x_train.astype('float32') / 255) # Normalize the images to [0, 1]
	if colorMode == "gray":
		x_train = np.expand_dims(x_train, axis=3) # add extra dimensionality
	return x_train

def load_dataset(path, colorMode="color", width=None, height=None, normalization=1):
	print("Dataset from:", path)
	if width == None and height == None:
		imgsArray = loadImgsArray(path, colorMode=colorMode, width=28, height=28)
	elif type(width) == int and type(height) == int:
		imgsArray = loadImgsArray(path, colorMode=colorMode, width=width, height=height)
	if normalization == 1:
		imgsArray = (imgsArray.astype('float32') - 127.5) / 127.5 # Normalize the images to [-1, 1]
	elif normalization == 2:
		imgsArray = (imgsArray.astype('float32') / 255) # Normalize the images to [0, 1]
	if colorMode == "gray":
		imgsArray = np.expand_dims(imgsArray, axis=3) # add extra dimensionality
	return imgsArray

# Other functions
# ------------------------------
def convert_secs_to_hms(seconds):
    minutes = seconds // 60
    hours = minutes // 60
    return ("%02d:%02d:%02d" % (hours, minutes % 60, seconds % 60))