from tensorflow.keras.datasets import cifar10, mnist, fashion_mnist
from tensorflow.keras.layers import Input, Dense, Reshape, Flatten, Dropout, multiply, GaussianNoise
from tensorflow.keras.layers import BatchNormalization, Activation, Embedding, ZeroPadding2D
from tensorflow.keras.layers import MaxPooling2D, Lambda # , merge
from tensorflow.keras.layers import LeakyReLU
from tensorflow.keras.layers import UpSampling2D, Conv2D
from tensorflow.keras.models import Sequential, Model
from tensorflow.keras.optimizers import Adam
from tensorflow.keras import losses
from tensorflow.keras.utils import to_categorical
import keras.backend as K
import tensorflow as tf
from tensorflow import keras

import matplotlib.pyplot as plt
import numpy as np
import csv
import time

from utils import *

#from abc import ABC, abstractmethod

"""
class MyModel(ABC):
	def __init__(self):
		pass
	@abstractmethod
	def train(self):
		pass
"""

class AAE(keras.Model):
	def __init__(self, **kwargs):
		super(AAE, self).__init__(name="AAE",**kwargs)
		self.trainingGeneratedImagesPath = None
		self.resultsPath = None
		self.resultsCsvFilename = None
		self.modelsPath = None
		self.plotsPath = None
		self.imgsNb = None
		self.width = None
		self.height = None
		self.channels = None
		self.img_shape = None
		self.latent_dim = None
		#self.optimizer = None
		self.optimizerD = None
		self.optimizerAE = None
		self.discriminator = None
		self.encoder = None
		self.decoder = None
		self.adversarial_autoencoder = None
		self.cross_entropy = None
		self.mse = None
		self.batch_size = 32
		self.d_loss_tracker = keras.metrics.Mean(name="d_loss")
		self.g_loss_tracker = keras.metrics.Mean(name="g_loss")
		self.d_accuracy_real_tracker = keras.metrics.Mean(name="d_accuracy_real")
		self.d_accuracy_fake_tracker = keras.metrics.Mean(name="d_accuracy_fake")
		self.d_accuracy_tracker = keras.metrics.Mean(name="d_accuracy")
		self.dataset_name = None

	def compile(self, imgs_array, dataset_name, latent_dim):
		super(AAE, self).compile()
		self.dataset_name = dataset_name
		fromLogits = False
		self.latent_dim = latent_dim
		self.trainingGeneratedImagesPath = "./Training_Generated_Images/"+dataset_name+"/"+self.name+"/"
		self.resultsPath = "./Results/"+dataset_name+"/"
		self.resultsCsvFilename = "results_" + self.name +"_latent_dim_"+str(self.latent_dim)+".csv"
		self.modelsPath = "./Models/"+dataset_name+"/"
		self.plotsPath = "./Plots/"+dataset_name+"/"
		self.initialize_directories()
		self.imgsNb, self.width, self.height, self.channels = imgs_array.shape
		self.img_shape = (self.width, self.height, self.channels)
		#self.optimizer = Adam(learning_rate=0.0002, beta_1=0.5)
		self.optimizerD = Adam(learning_rate=0.0002, beta_1=0.5)
		self.optimizerAE = Adam(learning_rate=0.0002, beta_1=0.5)
		self.cross_entropy = tf.keras.losses.BinaryCrossentropy(from_logits=fromLogits)
		self.mse = tf.keras.losses.MeanSquaredError()
		# Build and compile the discriminator
		self.discriminator = self.build_discriminator()
		#self.discriminator.compile(loss='binary_crossentropy', optimizer=self.optimizer, metrics=['accuracy'])
		# Build the encoder / decoder
		self.encoder = self.build_encoder()
		self.decoder = self.build_decoder()
		img = Input(shape=self.img_shape)
		# The generator takes the image, encodes it and reconstructs it
		# from the encoding
		encoded_repr = self.encoder(img)
		reconstructed_img = self.decoder(encoded_repr)
		# For the adversarial_autoencoder model we will only train the generator
		self.discriminator.trainable = False
		# The discriminator determines validity of the encoding
		validity = self.discriminator(encoded_repr)
		# The adversarial_autoencoder model  (stacked generator and discriminator)
		self.adversarial_autoencoder = Model(img, [reconstructed_img, validity])
		#self.adversarial_autoencoder.compile(loss=['mse', 'binary_crossentropy'], loss_weights=[0.999, 0.001], optimizer=self.optimizer)

	def getDiscriminatorLoss(self, real_output, fake_output):
		real_loss = self.cross_entropy(tf.ones_like(real_output), real_output)
		fake_loss = self.cross_entropy(tf.zeros_like(fake_output), fake_output)
		total_loss = real_loss + fake_loss
		return real_loss, fake_loss, total_loss

	def get_AE_loss(self, ae_output, real, weight_loss_1=0.999, weight_loss_2=0.001):
		ae_loss_1 = self.mse(real, ae_output)
		ae_loss_2 = self.cross_entropy(tf.ones_like(ae_output), ae_output)
		total_loss = ae_loss_1 * weight_loss_1 + ae_loss_2 * weight_loss_2
		return total_loss

	@tf.function
	def train_step(self, images):
		# images is a batch/a sub-dataset
		with tf.GradientTape() as ae_tape, tf.GradientTape() as disc_tape:
			latent_fake = self.encoder(images, training=False)
			latent_real = np.random.normal(size=(self.batch_size, self.latent_dim))
			real_output = self.discriminator(latent_real, training=True)
			fake_output = self.discriminator(latent_fake, training=True)
			d_loss_real, d_loss_fake, d_loss = self.getDiscriminatorLoss(real_output, fake_output)
			d_loss = tf.reduce_mean(d_loss)
			d_accuracy_real = tf.math.reduce_sum(real_output)/tf.cast(len(real_output), tf.float32)
			d_accuracy_fake = tf.math.reduce_sum(fake_output)/tf.cast(len(fake_output), tf.float32)
			d_accuracy = tf.math.reduce_mean([d_accuracy_real, d_accuracy_fake])
			ae_output, _ = self.adversarial_autoencoder(images, training=True)
			g_loss = self.get_AE_loss(ae_output, images)
			g_loss = tf.reduce_mean(g_loss)
		gradients_of_generator = ae_tape.gradient(g_loss, self.adversarial_autoencoder.trainable_variables)
		gradients_of_discriminator = disc_tape.gradient(d_loss, self.discriminator.trainable_variables)
		self.optimizerAE.apply_gradients(zip(gradients_of_generator, self.adversarial_autoencoder.trainable_variables))
		self.optimizerD.apply_gradients(zip(gradients_of_discriminator, self.discriminator.trainable_variables))
		self.d_loss_tracker.update_state(d_loss)
		self.g_loss_tracker.update_state(g_loss)
		self.d_accuracy_real_tracker.update_state(d_accuracy_real)
		self.d_accuracy_fake_tracker.update_state(d_accuracy_fake)
		self.d_accuracy_tracker.update_state(d_accuracy)
		return {"d_loss": self.d_loss_tracker.result(), "g_loss": self.g_loss_tracker.result(), 
			"d_accuracy_real": self.d_accuracy_real_tracker.result(), "d_accuracy_fake": self.d_accuracy_fake_tracker.result(),
			"d_accuracy": self.d_accuracy_tracker.result()}

	def build_encoder(self):
		# Encoder
		img = Input(shape=self.img_shape)
		h = Flatten()(img)
		h = Dense(512)(h)
		h = LeakyReLU(alpha=0.2)(h)
		h = Dense(512)(h)
		h = LeakyReLU(alpha=0.2)(h)
		mu = Dense(self.latent_dim)(h)
		log_var = Dense(self.latent_dim)(h)
		#latent_repr = merge([mu, log_var], mode=lambda p: p[0] + K.random_normal(K.shape(p[0])) * K.exp(p[1] / 2), output_shape=lambda p: p[0])
		latent_repr = Lambda(self.sampling, output_shape=(self.latent_dim,), name='z')([mu, log_var])
		return Model(img, latent_repr)

	def sampling(self, args):
		z_mean, z_log_var = args
		batch = K.shape(z_mean)[0]
		dim = K.int_shape(z_mean)[1]
		# by default, random_normal has mean=0 and std=1.0
		epsilon = K.random_normal(shape=(batch, dim))
		return z_mean + K.exp(0.5 * z_log_var) * epsilon

	def build_decoder(self):
		model = Sequential(name="Decoder")
		model.add(Dense(512, input_dim=self.latent_dim))
		model.add(LeakyReLU(alpha=0.2))
		model.add(Dense(512))
		model.add(LeakyReLU(alpha=0.2))
		model.add(Dense(np.prod(self.img_shape), activation='tanh'))
		model.add(Reshape(self.img_shape))
		model.summary()
		z = Input(shape=(self.latent_dim,))
		img = model(z)
		return Model(z, img)

	def build_discriminator(self):
		model = Sequential(name="Discriminator")
		model.add(Dense(512, input_dim=self.latent_dim))
		model.add(LeakyReLU(alpha=0.2))
		model.add(Dense(256))
		model.add(LeakyReLU(alpha=0.2))
		model.add(Dense(1, activation="sigmoid"))
		model.summary()
		encoded_repr = Input(shape=(self.latent_dim, ))
		validity = model(encoded_repr)
		return Model(encoded_repr, validity)

	def save_results(self, epoch, resultsList, sample_interval):
		loss_interval = 1
		if epoch % loss_interval == 0:
			with open(self.resultsPath + self.resultsCsvFilename, "a", newline='') as csvfile:
				writer = csv.writer(csvfile, delimiter=';')
				writer.writerow([epoch]+resultsList)
		if epoch % sample_interval == 0: # Save generated image samples
			self.sample_images(epoch)

	def sample_images(self, epoch, n=10):
		noise = np.random.normal(size=(n, self.latent_dim))
		gen_imgs = self.decoder.predict(noise)
		# Rescale images 0 - 1
		#gen_imgs = 0.5 * gen_imgs + 0.5
		fig, axs = plt.subplots(nrows=1, ncols=n)
		cnt = 0
		for j in range(n):
			imgs = gen_imgs[cnt] = (gen_imgs[cnt]) * 127.5 + 127.5
			imgs = imgs.astype(np.uint8)
			if self.channels == 3:
				axs[j].imshow(imgs)
			elif self.channels == 1:
				axs[j].imshow(imgs, cmap='gray')
			axs[j].axis('off')
			cnt += 1
		axs[0].set_title("Epoch " + str(epoch))
		path = self.trainingGeneratedImagesPath+"latent_dim_"+str(self.latent_dim)+"/"
		fig.savefig(path+"%d.png" % epoch, bbox_inches='tight')
		plt.close()

	def plot_latent(self):
		# display a n * n 2D manifold of images
		n = 10
		img_dim = self.width
		img_channel = self.channels
		scale = 2.0
		figsize = 15
		figure = np.zeros((img_dim * n, img_dim * n, img_channel))
		# linearly spaced coordinates corresponding to the 2D plot
		# of img classes in the latent space
		grid_x = np.linspace(-scale, scale, n)
		grid_y = np.linspace(-scale, scale, n)[::-1]
		for i, yi in enumerate(grid_y):
			for j, xi in enumerate(grid_x):
				z_sample = np.array([[xi, yi]])
				x_decoded = self.decoder.predict(z_sample)
				x_decoded = (x_decoded) * 127.5 + 127.5
				x_decoded = x_decoded.astype(np.uint8)
				img = x_decoded[0].reshape(img_dim, img_dim, img_channel)
				figure[
					i * img_dim : (i + 1) * img_dim,
					j * img_dim : (j + 1) * img_dim,
				] = img
		plt.figure(figsize=(figsize, figsize))
		plt.grid(b=None)
		start_range = img_dim // 2
		end_range = n * img_dim + start_range
		pixel_range = np.arange(start_range, end_range, img_dim)
		sample_range_x = np.round(grid_x, 1)
		sample_range_y = np.round(grid_y, 1)
		plt.xticks(pixel_range, sample_range_x)
		plt.yticks(pixel_range, sample_range_y)
		plt.xlabel("z[0]")
		plt.ylabel("z[1]")
		figure = figure.astype(np.uint8)
		if self.channels == 3:
			plt.imshow(figure)
		elif self.channels == 1:
			plt.imshow(figure, cmap='gray')
		plt.savefig(self.plotsPath + self.dataset_name + "_" + self.name +'_latent.png', bbox_inches='tight', pad_inches=0)
		#plt.show()

	def generate_imgs(self, imgsNb=10):
		noise = np.random.normal(size=(imgsNb, self.latent_dim))
		gen_imgs = self.decoder.predict(noise)
		gen_imgs = (gen_imgs) * 127.5 + 127.5
		gen_imgs = gen_imgs.astype(np.uint8)
		gen_imgs = np.squeeze(gen_imgs)
		return gen_imgs

	def generate_imgs_to(self, path, imgsNb=10):
		genImgs = self.generate_imgs(imgsNb)
		if self.channels == 1:
			genImgsPIL = convertToImgPIL(genImgs, colorMode="gray")
		elif self.channels == 3:
			genImgsPIL = convertToImgPIL(genImgs, colorMode="color")
		genImgsPIL = convertToImgPIL(genImgs)
		emptyDirectory(path)
		createDirectory(path)
		saveImgsPIL(genImgsPIL, path)
		#self.generate_gif(genImgsPIL, path)

	def generate_gif(self, imgsPIL, path):
		generateGif(imgsPIL, path, filename=self.name+"_latent_dim_"+str(self.latent_dim))

	def initialize_directories(self):
		createDirectory(self.trainingGeneratedImagesPath+"latent_dim_"+str(self.latent_dim)+"/")
		createDirectory(self.resultsPath)
		removeFile(self.resultsPath + self.resultsCsvFilename)
		emptyDirectory(self.trainingGeneratedImagesPath+"latent_dim_"+str(self.latent_dim)+"/")
		createDirectory(self.plotsPath)

	def save_decoder(self):
		createDirectory(self.modelsPath)
		self.decoder.save(self.modelsPath+self.name+"_latent_dim_"+str(self.latent_dim)+"_decoder.h5")

def trainAAE(imgs_array, dataset_name, latent_dim=2, epochs=10, batch_size=32):
	np.random.seed(1)
	tf.random.set_seed(1)
	print(imgs_array.shape)
	generativeModelName="AAE"
	aae = AAE()
	aae.compile(imgs_array, dataset_name, latent_dim)
	f = open("training_time.txt", "a")
	start = time.time()
	#aae.train(imgs_array, dataset_name, latent_dim=latent_dim, epochs=epochs, batch_size=batch_size)
	#batches = tf.data.Dataset.from_tensor_slices(imgs_array).shuffle(len(imgs_array)).batch(batch_size, drop_remainder=True)
	#batches = batches.take(100)
	history = aae.fit(imgs_array, epochs=epochs, batch_size=32, verbose=2)
	csvfile = open(aae.resultsPath + aae.resultsCsvFilename, 'w', newline='')
	writer = csv.writer(csvfile, delimiter=';')
	print(history.history.keys())
	for epoch in range(len(history.history['d_loss'])):
		d_loss = history.history['d_loss'][epoch]
		g_loss = history.history['g_loss'][epoch]
		d_accuracy_real = history.history['d_accuracy_real'][epoch]
		d_accuracy_fake = history.history['d_accuracy_fake'][epoch]
		d_accuracy = history.history['d_accuracy'][epoch]
		print("epoch", epoch+1, "d_loss", d_loss, "g_loss", g_loss, 
			"d_accuracy_real", d_accuracy_real, "d_accuracy_fake", d_accuracy_fake, "d_accuracy", d_accuracy)
		resultsList = [d_loss, 100*d_accuracy, 100*d_accuracy_real, 100*d_accuracy_fake, g_loss]
		writer.writerow([epoch+1]+resultsList)
	csvfile.close()
	aae.save_decoder()
	if latent_dim == 2:
		aae.plot_latent()
	end = time.time()
	elapsedTime_secs = end - start
	elapsedTime_hms = convert_secs_to_hms(elapsedTime_secs)
	print("Elapsed time:", elapsedTime_hms)
	f.write(generativeModelName+", "+dataset_name+", Epochs "+str(epochs)+", Batch Size "+str(batch_size)+", Latent Dim "+str(latent_dim)+", Training Time "+elapsedTime_hms+"\n")
	f.close()
	path = "./Generated_Images/"+dataset_name+"/"+generativeModelName+"_latent_dim_"+str(latent_dim)+"/"
	aae.generate_imgs_to(path, 100)


if __name__ == '__main__':
	"""
	imgs_array = load_tf_dataset()
	imgs_array = load_dataset(path="./bob_ross_dataset_28x28/")
	imgs_array = load_dataset(path="./pokemons_dataset_28x28/")
	imgs_array = load_dataset(path="./anime_faces_dataset_28x28/")
	imgs_array = load_dataset(path="./cat_faces_dataset_28x28/")
	imgs_array = load_dataset(path="./human_faces_dataset_28x28/")
	imgs_array = load_dataset(path="./abstract_paintings_dataset_28x28/")
	imgs_array = load_dataset(path="./flowers_dataset_28x28/")
	imgs_array = load_dataset(path="./sunflowers_dataset_28x28/")
	imgs_array = load_dataset(path="./celebrities_faces_dataset_28x28/")
	imgs_array = load_dataset(path="./simpsons_faces_dataset_28x28/")
	imgs_array = load_dataset(path="./triangles_dataset_28x28/")
	imgs_array = load_dataset(path="./shapes_dataset_28x28/")
	imgs_array = load_dataset(path="./birds_dataset_28x28/")
	imgs_array = load_dataset(path="./animal_faces_dataset_28x28/")
	imgs_array = load_tf_dataset(classNbs=[0])
	"""
	"""
	#imgs_array = load_tf_dataset(datasetNb=2, colorMode="color")
	datasetsPath = "./Datasets/"
	dataset_name = "anime_faces_dataset_28x28"
	imgs_array = load_dataset(path=datasetsPath+dataset_name+"/")
	trainAAE(imgs_array, dataset_name, latent_dim=2, epochs=10, batch_size=32)
	trainAAE(imgs_array, dataset_name, latent_dim=10, epochs=10, batch_size=32)
	"""

	imgs_array = load_tf_dataset(datasetNb=2)
	dataset_name = "Fashion_MNIST"
	trainAAE(imgs_array, dataset_name, latent_dim=2, epochs=20, batch_size=32)
	#trainAAE(imgs_array, dataset_name, latent_dim=10, epochs=2, batch_size=32)

	"""
	datasetsPath = "./Datasets/"
	dataset_name = "human_faces_dataset_28x28"
	imgs_array = load_dataset(path=datasetsPath+dataset_name+"/")
	trainAAE(imgs_array, dataset_name, latent_dim=2, epochs=1000, batch_size=32)
	trainAAE(imgs_array, dataset_name, latent_dim=10, epochs=1000, batch_size=32)
	"""