import tensorflow as tf
from tensorflow.keras.datasets import cifar10
import numpy as np
import matplotlib.pyplot as plt
from utils import *

def saveRealImages(imgsArray, dataset_name, imgsNb=100, colorMode="color"):
	np.random.shuffle(imgsArray)
	realImgs = imgsArray[:imgsNb]
	print(realImgs.shape)
	#realImgs = resizeImgsArray(realImgs, width=28, height=28, colorMode="color")
	#realImgsPIL = convertToImgPIL(realImgs, colorMode=colorMode)
	realImgsPath = "./Real_Images/"+dataset_name+"/"
	emptyDirectory(realImgsPath)
	createDirectory(realImgsPath)
	#saveImgsPIL(realImgsPIL, realImgsPath, colorMode=colorMode)
	saveImgsArray(realImgsPath, imgsArray)
	return realImgs

def loadModel(path):
	model = tf.keras.models.load_model(path, compile=False)
	return model

def generateImgsFromGAN(generator, imgsNb=10):
	latent_dim = 100
	noise = np.random.normal(0, 1, (imgsNb, latent_dim))
	gen_imgs = generator.predict(noise)
	gen_imgs = (gen_imgs) * 127.5 + 127.5
	gen_imgs = gen_imgs.astype(np.uint8)
	gen_imgs = np.squeeze(gen_imgs)
	return gen_imgs

def generateImgsFromAAE(decoder, imgsNb=10, latent_dim=10):
	z = np.random.normal(size=(imgsNb, latent_dim))
	gen_imgs = decoder.predict(z)
	gen_imgs = (gen_imgs) * 127.5 + 127.5
	gen_imgs = gen_imgs.astype(np.uint8)
	gen_imgs = np.squeeze(gen_imgs)
	return gen_imgs

def generateImgsFromVAE(decoder, imgsNb=10, latent_dim=2):
	gen_imgs = decoder.predict(np.random.normal(size=(imgsNb, latent_dim)))
	gen_imgs = gen_imgs * 255
	gen_imgs = gen_imgs.astype(np.uint8)
	gen_imgs = np.squeeze(gen_imgs)
	return gen_imgs

def saveImgs(genImgsArray, generatedImagesPath, genModelName, colorMode="color"):
	path=generatedImagesPath+genModelName+"/"
	genImgsPIL = convertToImgPIL(genImgsArray, colorMode=colorMode)
	createDirectory(path)
	saveImgsPIL(genImgsPIL, path)

def generateImgs(imgs_array, dataset_name, colorMode="color", imgsNb = 1000):
	imgsNb = len(imgs_array)
	imgs_array = imgs_array * 127.5 + 127.5
	imgs_array = imgs_array.astype(np.uint8)
	imgs_array = np.squeeze(imgs_array)
	plotsPath = "./Plots/"+dataset_name+"/"
	modelsPath= "./Models/"+dataset_name+"/"
	createDirectory(plotsPath)
	generatedImagesPath = "./Generated_Images2/"+dataset_name+"/"
	createDirectory(generatedImagesPath)
	ganGen = loadModel(modelsPath+'GAN_generator.h5')
	np.random.seed(1)
	genImgsGAN = generateImgsFromGAN(ganGen, imgsNb)
	saveImgs(genImgsGAN, generatedImagesPath, "GAN", colorMode=colorMode)
	
	ganGen = loadModel(modelsPath+'DCGAN_generator.h5')
	np.random.seed(1)
	genImgsDCGAN = generateImgsFromGAN(ganGen, imgsNb)
	saveImgs(genImgsDCGAN, generatedImagesPath, "DCGAN", colorMode=colorMode)

	ganGen = loadModel(modelsPath+'WGAN_generator.h5')
	np.random.seed(1)
	genImgsWGAN = generateImgsFromGAN(ganGen, imgsNb)
	saveImgs(genImgsWGAN, generatedImagesPath, "WGAN", colorMode=colorMode)

	aaeDecoder = loadModel(modelsPath+'AAE_latent_dim_2_decoder.h5')
	np.random.seed(1)
	genImgsAAE_LD_2 = generateImgsFromAAE(aaeDecoder, imgsNb, latent_dim=2)
	saveImgs(genImgsAAE_LD_2, generatedImagesPath, "AAE_latent_dim_2", colorMode=colorMode)

	aaeDecoder = loadModel(modelsPath+'AAE_latent_dim_10_decoder.h5')
	np.random.seed(1)
	genImgsAAE_LD_10 = generateImgsFromAAE(aaeDecoder, imgsNb, latent_dim=10)
	saveImgs(genImgsAAE_LD_10, generatedImagesPath, "AAE_latent_dim_10", colorMode=colorMode)

	vaeDecoder = loadModel(modelsPath+'VAE_latent_dim_2_decoder.h5')
	np.random.seed(1)
	genImgsVAE_LD_2 = generateImgsFromVAE(vaeDecoder, imgsNb, latent_dim=2)
	saveImgs(genImgsVAE_LD_2, generatedImagesPath, "VAE_latent_dim_2", colorMode=colorMode)

	vaeDecoder = loadModel(modelsPath+'VAE_latent_dim_10_decoder.h5')
	np.random.seed(1)
	genImgsVAE_LD_10 = generateImgsFromVAE(vaeDecoder, imgsNb, latent_dim=10)
	saveImgs(genImgsVAE_LD_10, generatedImagesPath, "VAE_latent_dim_10", colorMode=colorMode)

	np.random.seed(1)
	realImgs = saveRealImages(imgs_array[:imgsNb], dataset_name, imgsNb, colorMode=colorMode)
	"""
	# Make a figure with the images generated in order to compare the generative models.
	n = 10
	imgsList = [realImgs[:n], genImgsGAN[:n], genImgsDCGAN[:n], genImgsWGAN[:n], genImgsAAE_LD_2[:n], genImgsAAE_LD_10[:n], genImgsVAE_LD_2[:n], genImgsVAE_LD_10[:n]]
	rows = ["Real", "GAN", "DCGAN", "WGAN", "AAE_LD_2", "AAE_LD_10", "VAE_LD_2", "VAE_LD_10"]
	f, axis = plt.subplots(nrows=8, ncols=n, figsize=(16, 8), sharey=True)
	#plt.subplots_adjust(bottom=None, right=None, top=None, wspace=None, hspace=0.4)
	for i, ax in enumerate(axis):
		for j, col in enumerate(ax):
			#col.axis('off')
			col.set_xticklabels([])
			col.set_yticks([])
			col.set_xticks([])
			col.spines['top'].set_visible(False)
			col.spines['right'].set_visible(False)
			col.spines['bottom'].set_visible(False)
			col.spines['left'].set_visible(False)
			if colorMode == "color":
				col.imshow(imgsList[i][j])
			elif colorMode == "gray":
				col.imshow(imgsList[i][j], cmap="gray")
	for ax, row in zip(axis[:,0], rows):
		ax.set_ylabel(row+"                 ", rotation=0, size='large')
	plt.savefig(plotsPath+ dataset_name + '_' +'results-comparison.png', bbox_inches='tight', pad_inches=0)
	"""
	
if __name__ == '__main__':
	
	datasetsPath = "../Datasets/"
	dataset_name = "anime_faces_dataset_28x28"
	path = datasetsPath+dataset_name+"/"
	imgs_array = load_dataset(path=path, colorMode="color")
	generateImgs(imgs_array, dataset_name, colorMode="color")
	
	dataset_name = "Fashion_MNIST"
	imgs_array = load_tf_dataset(datasetNb=2, colorMode="gray")
	generateImgs(imgs_array, dataset_name, colorMode="gray")

	dataset_name = "MNIST"
	imgs_array = load_tf_dataset(datasetNb=1, colorMode="gray")
	generateImgs(imgs_array, dataset_name, colorMode="gray")