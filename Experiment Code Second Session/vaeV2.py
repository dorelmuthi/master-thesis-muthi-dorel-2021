import numpy as np
import tensorflow as tf
from tensorflow.keras.datasets import cifar10, mnist, fashion_mnist
from tensorflow import keras
from tensorflow.keras import Input, Model
from tensorflow.keras.layers import Layer, Conv2D, Flatten, Dense, Reshape, Conv2DTranspose
import matplotlib.pyplot as plt
import time
from utils import *

# this sampling layer is the bottleneck layer of variational autoencoder,
# it uses the output from two dense layers z_mean and z_log_var as input,
# convert them into normal distribution and pass them to the decoder layer
class Sampling(Layer):
	def call(self, inputs):
		z_mean, z_log_var = inputs
		batch = tf.shape(z_mean)[0]
		dim = tf.shape(z_mean)[1]
		epsilon = tf.keras.backend.random_normal(shape =(batch, dim))
		return z_mean + tf.exp(0.5 * z_log_var) * epsilon

# define the complete variational autoencoder architecture
class VAE(keras.Model):
	def __init__(self, **kwargs):
		super(VAE, self).__init__(name="VAE",**kwargs)
		self.trainingGeneratedImagesPath = None
		self.resultsPath = None
		self.resultsCsvFilename = None
		self.modelsPath = None
		self.plotsPath = None
		self.imgsNb = None
		self.width = None
		self.height = None
		self.channels = None
		self.img_shape = None
		self.latent_dim = None
		self.encoder = None
		self.decoder = None
		self.loss_tracker = keras.metrics.Mean(name="loss")
		self.reconstruction_loss_tracker = keras.metrics.Mean(name="reconstruction_loss")
		self.kl_loss_tracker = keras.metrics.Mean(name="kl_loss")
		self.dataset_name = None

	def initialize(self, imgs_array, dataset_name, latent_dim):
		self.dataset_name = dataset_name
		self.latent_dim = latent_dim
		self.trainingGeneratedImagesPath = "./Training_Generated_Images/"+dataset_name+"/"+self.name+"/"
		self.resultsPath = "./Results/"+dataset_name+"/"
		self.resultsCsvFilename = "results_" + self.name +"_latent_dim_"+str(self.latent_dim)+".csv"
		self.modelsPath = "./Models/"+dataset_name+"/"
		self.plotsPath = "./Plots/"+dataset_name+"/"
		self.initialize_directories()
		self.imgsNb, self.width, self.height, self.channels = imgs_array.shape
		self.img_shape = (self.width, self.height, self.channels)
		self.encoder = self.build_encoder()
		self.decoder = self.build_decoder()

	def build_encoder(self):
		encoder_inputs = Input(shape = self.img_shape)
		x = Conv2D(32, 3, activation ="relu", strides = 2, padding ="same")(encoder_inputs)
		x = Conv2D(64, 3, activation ="relu", strides = 2, padding ="same")(x)
		x = Flatten()(x)
		x = Dense(16, activation ="relu")(x)
		z_mean = Dense(self.latent_dim, name ="z_mean")(x)
		z_log_var = Dense(self.latent_dim, name ="z_log_var")(x)
		z = Sampling()([z_mean, z_log_var])
		encoder = Model(encoder_inputs, [z_mean, z_log_var, z], name ="encoder")
		encoder.summary()
		return encoder

	def build_decoder(self):
		# Define Decoder Architecture
		latent_inputs = keras.Input(shape =(self.latent_dim, ))
		x = Dense(7 * 7 * 64, activation ="relu")(latent_inputs)
		x = Reshape((7, 7, 64))(x)
		x = Conv2DTranspose(64, 3, activation ="relu", strides = 2, padding ="same")(x)
		x = Conv2DTranspose(32, 3, activation ="relu", strides = 2, padding ="same")(x)
		decoder_outputs = Conv2DTranspose(self.channels, 3, activation ="sigmoid", padding ="same")(x)
		decoder = Model(latent_inputs, decoder_outputs, name ="decoder")
		decoder.summary()
		return decoder

	def train_step(self, data):
		if isinstance(data, tuple):
			data = data[0]
		with tf.GradientTape() as tape:
			z_mean, z_log_var, z = self.encoder(data)
			reconstruction = self.decoder(z)
			reconstruction_loss = tf.reduce_mean(keras.losses.binary_crossentropy(data, reconstruction))
			reconstruction_loss *= self.width * self.height
			kl_loss = 1 + z_log_var - tf.square(z_mean) - tf.exp(z_log_var)
			kl_loss = tf.reduce_mean(kl_loss)
			kl_loss *= -0.5
			total_loss = reconstruction_loss + kl_loss
		grads = tape.gradient(total_loss, self.trainable_weights)
		self.optimizer.apply_gradients(zip(grads, self.trainable_weights))
		self.loss_tracker.update_state(total_loss)
		self.reconstruction_loss_tracker.update_state(reconstruction_loss)
		self.kl_loss_tracker.update_state(kl_loss)
		#return {"loss": total_loss,"reconstruction_loss": reconstruction_loss, "kl_loss": kl_loss,}
		return {"loss": self.loss_tracker.result(),"reconstruction_loss": reconstruction_loss, "kl_loss": kl_loss,}

	# imgs_array must be normalized to [0, 1]
	def train(self, imgs_array, dataset_name, latent_dim=2, epochs=10, batch_size=32):
		self.initialize(imgs_array, dataset_name, latent_dim)
		# compile and train the model
		self.compile(optimizer ='rmsprop')
		history = self.fit(imgs_array, epochs = epochs, batch_size = batch_size, verbose=2)
		#print("history keys",history.history.keys())
		# save loss
		import csv
		csvfile = open(self.resultsPath + self.resultsCsvFilename, 'w', newline='')
		writer = csv.writer(csvfile, delimiter=';')
		for epoch in range(len(history.history['loss'])):
			loss = history.history['loss'][epoch]
			reconstruction_loss = history.history['reconstruction_loss'][epoch]
			kl_loss = history.history['kl_loss'][epoch]
			print("epoch", epoch+1, "loss", loss, "reconstruction_loss", reconstruction_loss, "kl_loss", kl_loss)
			writer.writerow([epoch+1, loss])
		csvfile.close()
		# VAE plots if latent_dim=2
		if self.latent_dim==2:
			self.plot_latent()
		self.save_decoder()
	
	def plot_latent(self):
		# display a n * n 2D manifold of images
		n = 10
		img_dim = self.width
		img_channel = self.channels
		scale = 2.0
		figsize = 15
		figure = np.zeros((img_dim * n, img_dim * n, img_channel))
		# linearly spaced coordinates corresponding to the 2D plot
		# of img classes in the latent space
		grid_x = np.linspace(-scale, scale, n)
		grid_y = np.linspace(-scale, scale, n)[::-1]
		for i, yi in enumerate(grid_y):
			for j, xi in enumerate(grid_x):
				z_sample = np.array([[xi, yi]])
				x_decoded = self.decoder.predict(z_sample)
				x_decoded = x_decoded * 255
				x_decoded = x_decoded.astype(np.uint8)
				img = x_decoded[0].reshape(img_dim, img_dim, img_channel)
				figure[
					i * img_dim : (i + 1) * img_dim,
					j * img_dim : (j + 1) * img_dim,
				] = img
		plt.figure(figsize=(figsize, figsize))
		plt.grid(b=None)
		start_range = img_dim // 2
		end_range = n * img_dim + start_range
		pixel_range = np.arange(start_range, end_range, img_dim)
		sample_range_x = np.round(grid_x, 1)
		sample_range_y = np.round(grid_y, 1)
		plt.xticks(pixel_range, sample_range_x)
		plt.yticks(pixel_range, sample_range_y)
		plt.xlabel("z[0]")
		plt.ylabel("z[1]")
		figure = figure.astype(np.uint8)
		if self.channels == 3:
			plt.imshow(figure)
		elif self.channels == 1:
			plt.imshow(figure, cmap='gray')
		plt.savefig(self.plotsPath + self.dataset_name + " " + self.name + '_latent.png', bbox_inches='tight', pad_inches=0)
		#plt.show()

	def generate_imgs(self, imgsNb=10):
		gen_imgs = self.decoder.predict(np.random.normal(size=(imgsNb, self.latent_dim)))
		gen_imgs = gen_imgs * 255
		gen_imgs = gen_imgs.astype(np.uint8)
		gen_imgs = np.squeeze(gen_imgs)
		return gen_imgs

	def generate_imgs_to(self, path, imgsNb=10):
		genImgs = self.generate_imgs(imgsNb)
		if self.channels == 1:
			genImgsPIL = convertToImgPIL(genImgs, colorMode="gray")
		elif self.channels == 3:
			genImgsPIL = convertToImgPIL(genImgs, colorMode="color")
		emptyDirectory(path)
		createDirectory(path)
		saveImgsPIL(genImgsPIL, path)
		#self.generate_gif(genImgsPIL, path)

	def generate_gif(self, imgsPIL, path):
		generateGif(imgsPIL, path, filename=self.name+"_latent_dim_"+str(self.latent_dim))

	def initialize_directories(self):
		createDirectory(self.resultsPath)
		removeFile(self.resultsPath + self.resultsCsvFilename)
		createDirectory(self.plotsPath)
		
	def save_decoder(self):
		createDirectory(self.modelsPath)
		self.decoder.save(self.modelsPath+self.name+"_latent_dim_"+str(self.latent_dim)+"_decoder.h5")

def trainVAE(imgs_array, dataset_name, latent_dim=2, epochs=10, batch_size=32):
	np.random.seed(1)
	tf.random.set_seed(1)
	print(imgs_array.shape)
	generativeModelName="VAE"
	vae = VAE()
	f = open("training_time.txt", "a")
	start = time.time()
	vae.train(imgs_array, dataset_name, latent_dim=latent_dim, epochs=epochs, batch_size=batch_size)
	end = time.time()
	elapsedTime_secs = end - start
	elapsedTime_hms = convert_secs_to_hms(elapsedTime_secs)
	print("Elapsed time:", elapsedTime_hms)
	f.write(generativeModelName+", "+dataset_name+", Epochs "+str(epochs)+", Batch Size "+str(batch_size)+", Latent Dim "+str(latent_dim)+", Training Time "+elapsedTime_hms+"\n")
	f.close()
	path = "./Generated_Images/"+dataset_name+"/"+generativeModelName+"_latent_dim_"+str(latent_dim)+"/"
	vae.generate_imgs_to(path, 100)

if __name__ == '__main__':
	"""
	imgs_array = load_tf_dataset()
	imgs_array = load_dataset(path="./bob_ross_dataset_28x28/")
	imgs_array = load_dataset(path="./pokemons_dataset_28x28/")
	imgs_array = load_dataset(path="./anime_faces_dataset_28x28/")
	imgs_array = load_dataset(path="./cat_faces_dataset_28x28/")
	imgs_array = load_dataset(path="./human_faces_dataset_28x28/")
	imgs_array = load_dataset(path="./abstract_paintings_dataset_28x28/")
	imgs_array = load_dataset(path="./flowers_dataset_28x28/")
	imgs_array = load_dataset(path="./sunflowers_dataset_28x28/")
	imgs_array = load_dataset(path="./celebrities_faces_dataset_28x28/")
	imgs_array = load_dataset(path="./simpsons_faces_dataset_28x28/")
	imgs_array = load_dataset(path="./triangles_dataset_28x28/")
	imgs_array = load_dataset(path="./shapes_dataset_28x28/")
	imgs_array = load_dataset(path="./birds_dataset_28x28/")
	imgs_array = load_dataset(path="./animal_faces_dataset_28x28/")
	imgs_array = load_tf_dataset(classNbs=[0])
	"""
	#imgs_array = load_tf_dataset(datasetNb=2, colorMode="color")
	
	"""
	datasetsPath = "./Datasets/"
	dataset_name = "anime_faces_dataset_28x28"
	imgs_array = load_dataset(path=datasetsPath+dataset_name+"/")
	#trainVAE(imgs_array, dataset_name, latent_dim=2, epochs=2, batch_size=32)
	trainVAE(imgs_array, dataset_name, latent_dim=10, epochs=2, batch_size=32)
	"""

	
	imgs_array = load_tf_dataset(datasetNb=2, normalization=2)
	dataset_name = "Fashion_MNIST"
	#trainVAE(imgs_array, dataset_name, latent_dim=2, epochs=2, batch_size=32)
	trainVAE(imgs_array, dataset_name, latent_dim=10, epochs=2, batch_size=32)
	

	"""
	datasetsPath = "./Datasets/"
	dataset_name = "human_faces_dataset_28x28"
	imgs_array = load_dataset(path=datasetsPath+dataset_name+"/")
	trainVAE(imgs_array, dataset_name, latent_dim=2, epochs=2, batch_size=32)
	trainVAE(imgs_array, dataset_name, latent_dim=10, epochs=2, batch_size=32)
	"""