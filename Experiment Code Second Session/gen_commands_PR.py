def printInstructionPR(dataset_name):
	string = "python prd_from_image_folders.py --inception_path ./inceptionModel/inception.pb --reference_dir" + \
	" ../Real_Images/" + dataset_name + "/" + \
	" --eval_dirs ../Generated_Images2/" + dataset_name + "/GAN/" + \
	" ../Generated_Images2/" + dataset_name + "/DCGAN/" + \
	" ../Generated_Images2/" + dataset_name + "/WGAN/" + \
	" ../Generated_Images2/" + dataset_name + "/AAE_latent_dim_10/" + \
	" ../Generated_Images2/" + dataset_name + "/VAE_latent_dim_10/" + \
	" --eval_labels GAN DCGAN WGAN AAE VAE"
	print(string)

printInstructionPR(dataset_name="anime_faces_dataset_28x28")
print(" ")
printInstructionPR(dataset_name="Fashion_MNIST")
print(" ")
printInstructionPR(dataset_name="MNIST")