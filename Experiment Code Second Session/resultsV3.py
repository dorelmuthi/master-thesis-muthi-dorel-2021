import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns; sns.set()
import tensorflow as tf

from utils import *

def getDataFrameFromCsv(path):
	col_names = ['epoch', 'd_loss', 'acc', 'acc_real', 'acc_fake', 'g_loss']
	dataFrame = pd.read_csv(path, delimiter=';', names=col_names)
	#df = pd.read_csv(resultPathAAE, delimiter=';', names=col_names, header=1) # header=1 to ignore header on the first line of csv
	return dataFrame

def loadModel(path):
	model = tf.keras.models.load_model(path, compile=False)
	return model

def plotLatent(plotsPath, dataset_name, generativeModelName, decoder, size, channels, normalization = 1):
	# display a n * n 2D manifold of images
	n = 10
	img_dim = size
	img_channel = channels
	scale = 2.0
	figsize = 15
	figure = np.zeros((img_dim * n, img_dim * n, img_channel))
	# linearly spaced coordinates corresponding to the 2D plot
	# of img classes in the latent space
	grid_x = np.linspace(-scale, scale, n)
	grid_y = np.linspace(-scale, scale, n)[::-1]
	for i, yi in enumerate(grid_y):
		for j, xi in enumerate(grid_x):
			z_sample = np.array([[xi, yi]])
			x_decoded = decoder.predict(z_sample)
			if normalization == 1:
				x_decoded = (x_decoded) * 127.5 + 127.5
			elif normalization == 2:
				x_decoded = x_decoded * 255
			x_decoded = x_decoded.astype(np.uint8)
			img = x_decoded[0].reshape(img_dim, img_dim, img_channel)
			figure[
				i * img_dim : (i + 1) * img_dim,
				j * img_dim : (j + 1) * img_dim,
			] = img
	plt.figure(figsize=(figsize, figsize))
	plt.grid(visible=None)
	start_range = img_dim // 2
	end_range = n * img_dim + start_range
	pixel_range = np.arange(start_range, end_range, img_dim)
	sample_range_x = np.round(grid_x, 1)
	sample_range_y = np.round(grid_y, 1)
	plt.xticks(pixel_range, sample_range_x)
	plt.yticks(pixel_range, sample_range_y)
	plt.xlabel("z[0]")
	plt.ylabel("z[1]")
	figure = figure.astype(np.uint8)
	if channels == 3:
		plt.imshow(figure)
	elif channels == 1:
		plt.imshow(figure, cmap='gray')
	plt.savefig(plotsPath + dataset_name + "_" + generativeModelName +'_latent.png', bbox_inches='tight', pad_inches=0)

def plotResultsComparison(plotsPath, imgsList, rows, colorMode="color"):
	# Make a figure with the images generated in order to compare the generative models.
	f, axis = plt.subplots(nrows=len(rows), ncols=len(imgsList[0]), figsize=(16, 8), sharey=True)
	#plt.subplots_adjust(bottom=None, right=None, top=None, wspace=None, hspace=0.4)
	for i, ax in enumerate(axis):
		for j, col in enumerate(ax):
			#col.axis('off')
			col.set_xticklabels([])
			col.set_yticks([])
			col.set_xticks([])
			col.spines['top'].set_visible(False)
			col.spines['right'].set_visible(False)
			col.spines['bottom'].set_visible(False)
			col.spines['left'].set_visible(False)
			if colorMode == "color":
				col.imshow(imgsList[i][j])
			elif colorMode == "gray":
				col.imshow(imgsList[i][j], cmap="gray")
	for ax, row in zip(axis[:,0], rows):
		ax.set_ylabel(row+"                 ", rotation=0, size='large')
	plt.savefig(plotsPath+ dataset_name + '_' +'results-comparison.png', bbox_inches='tight', pad_inches=0)

def plotResults(plotsPath, resultsPath, dataset_name):
	dataFrameGAN = getDataFrameFromCsv(resultsPath+"results_GAN.csv")
	dataFrameDCGAN = getDataFrameFromCsv(resultsPath+"results_DCGAN.csv")
	dataFrameWGAN = getDataFrameFromCsv(resultsPath+"results_WGAN.csv")
	dataFrameAAE_2 = getDataFrameFromCsv(resultsPath+"results_AAE_latent_dim_2.csv")
	dataFrameAAE_10 = getDataFrameFromCsv(resultsPath+"results_AAE_latent_dim_10.csv")

	latent_dim = 2
	resultsCsvFilename = "results_VAE_latent_dim_"+str(latent_dim)+".csv"
	path = resultsPath + resultsCsvFilename
	col_names = ['epoch', 'loss']
	dataFrameVAE_2 = pd.read_csv(path, delimiter=';', names=col_names)

	latent_dim = 10
	resultsCsvFilename = "results_VAE_latent_dim_"+str(latent_dim)+".csv"
	path = resultsPath + resultsCsvFilename
	col_names = ['epoch', 'loss']
	dataFrameVAE_10 = pd.read_csv(path, delimiter=';', names=col_names)

	fig, axes = plt.subplots(3, 3, sharex=True, figsize=(12, 12))
	# Row 1
	# Col 1
	s = sns.lineplot(ax=axes[0, 0], data=dataFrameGAN, x='epoch', y='d_loss', label='GAN', linestyle="solid")
	s = sns.lineplot(ax=axes[0, 0], data=dataFrameDCGAN, x='epoch', y='d_loss', label='DCGAN', linestyle="dashed")
	#s = sns.lineplot(ax=axes[0, 0], data=dataFrameWGAN, x='epoch', y='d_loss', label='WGAN', linestyle="dashdot")
	#s = sns.lineplot(ax=axes[0, 0], data=dataFrameAAE_2, x='epoch', y='d_loss', label='AAE', linestyle="dashdot")
	s = sns.lineplot(ax=axes[0, 0], data=dataFrameAAE_10, x='epoch', y='d_loss', label='AAE', linestyle="dotted")
	s.set_ylabel("Discriminator loss")
	# Col 2
	s = sns.lineplot(ax=axes[0, 1], data=dataFrameGAN, x='epoch', y='g_loss', label='GAN', linestyle="solid")
	s = sns.lineplot(ax=axes[0, 1], data=dataFrameDCGAN, x='epoch', y='g_loss', label='DCGAN', linestyle="dashed")
	#s = sns.lineplot(ax=axes[0, 1], data=dataFrameWGAN, x='epoch', y='g_loss', label='WGAN', linestyle="dashdot")
	#s = sns.lineplot(ax=axes[0, 1], data=dataFrameAAE_2, x='epoch', y='g_loss', label='AAE', linestyle="dashdot")
	s = sns.lineplot(ax=axes[0, 1], data=dataFrameAAE_10, x='epoch', y='g_loss', label='AAE', linestyle="dotted")
	s.set_ylabel("Generator loss")
	# Col 3
	s = sns.lineplot(ax=axes[0, 2], data=dataFrameVAE_10, x='epoch', y='loss', linestyle="solid")
	s.set_ylabel("VAE loss")
	# Row 2
	# Col 1
	s = sns.lineplot(ax=axes[1, 0], data=dataFrameGAN, x='epoch', y='acc_real', label='GAN', linestyle="solid")
	s = sns.lineplot(ax=axes[1, 0], data=dataFrameDCGAN, x='epoch', y='acc_real', label='DCGAN', linestyle="dashed")
	#s = sns.lineplot(ax=axes[1, 0], data=dataFrameWGAN, x='epoch', y='acc_real', label='WGAN', linestyle="dashdot")
	#s = sns.lineplot(ax=axes[1, 0], data=dataFrameAAE_2, x='epoch', y='acc_real', label='AAE', linestyle="dashdot")
	s = sns.lineplot(ax=axes[1, 0], data=dataFrameAAE_10, x='epoch', y='acc_real', label='AAE', linestyle="dotted")
	s.set_ylabel("Accuracy on real images")
	# Col 2
	s = sns.lineplot(ax=axes[1, 1], data=dataFrameGAN, x='epoch', y='acc_fake', label='GAN', linestyle="solid")
	s = sns.lineplot(ax=axes[1, 1], data=dataFrameDCGAN, x='epoch', y='acc_fake', label='DCGAN', linestyle="dashed")
	#s = sns.lineplot(ax=axes[1, 1], data=dataFrameWGAN, x='epoch', y='acc_fake', label='WGAN', linestyle="dashdot")
	#s = sns.lineplot(ax=axes[1, 1], data=dataFrameAAE_2, x='epoch', y='acc_fake', label='AAE', linestyle="dashdot")
	s = sns.lineplot(ax=axes[1, 1], data=dataFrameAAE_10, x='epoch', y='acc_fake', label='AAE', linestyle="dotted")	
	s.set_ylabel("Accuracy on fake images")	
	# Col 3
	s = sns.lineplot(ax=axes[1, 2], data=dataFrameGAN, x='epoch', y='acc', label='GAN', linestyle="solid")
	s = sns.lineplot(ax=axes[1, 2], data=dataFrameDCGAN, x='epoch', y='acc', label='DCGAN', linestyle="dashed")
	#s = sns.lineplot(ax=axes[1, 2], data=dataFrameWGAN, x='epoch', y='acc', label='WGAN', linestyle="dashdot")
	#s = sns.lineplot(ax=axes[1, 2], data=dataFrameAAE_2, x='epoch', y='acc', label='AAE', linestyle="dashdot")
	s = sns.lineplot(ax=axes[1, 2], data=dataFrameAAE_10, x='epoch', y='acc', label='AAE', linestyle="dotted")	
	s.set_ylabel("Accuracy avg. on real and fake images")	
	# Row 3
	# Col 1
	s = sns.lineplot(ax=axes[2, 0], data=dataFrameWGAN, x='epoch', y='d_loss', linestyle="solid")
	s.set_ylabel("Discriminator loss WGAN")	
	# Col 2
	s = sns.lineplot(ax=axes[2, 1], data=dataFrameWGAN, x='epoch', y='g_loss', linestyle="solid")
	s.set_ylabel("Generator loss WGAN")		
	# Col 3
	s = sns.lineplot(ax=axes[2, 2], data=dataFrameWGAN, x='epoch', y='acc_real', label='acc_real WGAN', linestyle="solid")
	s = sns.lineplot(ax=axes[2, 2], data=dataFrameWGAN, x='epoch', y='acc_fake', label='acc_fake WGAN', linestyle="solid")
	s = sns.lineplot(ax=axes[2, 2], data=dataFrameWGAN, x='epoch', y='acc', label='acc_avg WGAN', linestyle="solid")
	s.set_ylabel("Accuracies WGAN")

	plt.savefig(plotsPath + dataset_name + '_results.png', bbox_inches='tight', pad_inches=0, dpi=300)
	#plt.show()
	plt.close()

def makePlots(dataset_name, size=28, channels=1):
	if channels == 1:
		colorMode = "gray"
	elif channels == 3:
		colorMode = "color"
	sns.set(font_scale=1)
	p = sns.color_palette("bright") # (deep, muted, bright, pastel, dark, colorblind)
	sns.palplot(p)
	sns.set_palette(p)
	cmap = plt.cm.get_cmap('Spectral') # https://matplotlib.org/3.5.0/gallery/color/colormap_reference.html
	sns.set_context("notebook", font_scale=1, rc={"lines.linewidth": 2})

	plotsPath = "./Plots/"+dataset_name+"/"
	resultsPath = "./Results/"+dataset_name+"/"
	modelsPath= "./Models/"+dataset_name+"/"
	#emptyDirectory(plotsPath)
	createDirectory(plotsPath)

	plotResults(plotsPath, resultsPath, dataset_name)

	vaeDecoder = loadModel(modelsPath+'VAE_latent_dim_2_decoder.h5')
	aaeDecoder = loadModel(modelsPath+'AAE_latent_dim_2_decoder.h5')

	plotLatent(plotsPath, dataset_name, "VAE", vaeDecoder, size, channels, normalization = 2)
	plotLatent(plotsPath, dataset_name, "AAE", aaeDecoder, size, channels, normalization = 1)

	n = 10
	generatedImagesPath = "./Generated_Images2/"+dataset_name+"/"
	realImgsPath = "./Real_Images/"+dataset_name+"/"
	modelsName = ["GAN", "DCGAN", "WGAN", "AAE_latent_dim_2", "AAE_latent_dim_10", "VAE_latent_dim_2", "VAE_latent_dim_10"]
	realImgs = loadImgsArray2(realImgsPath, colorMode=colorMode, width=size, height=size, imgFormat="png", imgsNb=n)
	realImgs = realImgs[:n]
	imgsList = [realImgs]
	for modelName in modelsName:
		imgs = loadImgsArray2(generatedImagesPath+modelName+"/", colorMode=colorMode, width=size, height=size, imgFormat="png", imgsNb=n)
		imgsList.append(imgs[:n])
	rows = ["Real", "GAN", "DCGAN", "WGAN", "AAE_LD_2", "AAE_LD_10", "VAE_LD_2", "VAE_LD_10"]
	plotResultsComparison(plotsPath, imgsList, rows, colorMode=colorMode)
	
if __name__ == '__main__':
	dataset_name = "MNIST"
	makePlots(dataset_name, size=28, channels=1)

	dataset_name = "Fashion_MNIST"
	makePlots(dataset_name, size=28, channels=1)

	dataset_name = "anime_faces_dataset_28x28"
	makePlots(dataset_name, size=28, channels=3)