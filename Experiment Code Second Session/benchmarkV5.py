from ganV5 import *
from dcganV5 import *
from wganV5 import *
from aaeV5 import *
from vaeV2 import *
from resultsV2 import *
from generateImgsV2 import *
from utils import *

def benchmark_tf_dataset(epochs, datasetNb, colorMode, width=None, height=None, classNbs = []):
	start = time.time()
	imgs_array = load_tf_dataset(datasetNb=datasetNb, colorMode=colorMode, width=width, height=height, classNbs=classNbs)
	imgs_array2 = load_tf_dataset(datasetNb=datasetNb, colorMode=colorMode, width=width, height=height, classNbs=classNbs, normalization=2)
	if datasetNb == 1:
		dataset_name = "MNIST"	
	elif datasetNb == 2:
		dataset_name = "Fashion_MNIST"
	elif datasetNb == 3:
		dataset_name = "CIFAR_10"
	if classNbs != []:
		dataset_name += "_"+str(classNbs)
	trainGAN(imgs_array, dataset_name, epochs=epochs, batch_size=32)
	trainDCGAN(imgs_array, dataset_name, epochs=epochs, batch_size=32)
	trainWGAN(imgs_array, dataset_name, epochs=epochs, batch_size=32)
	trainAAE(imgs_array, dataset_name, latent_dim=2, epochs=epochs, batch_size=32)
	trainAAE(imgs_array, dataset_name, latent_dim=10, epochs=epochs, batch_size=32)
	trainVAE(imgs_array2, dataset_name, latent_dim=2, epochs=epochs, batch_size=32)
	trainVAE(imgs_array2, dataset_name, latent_dim=10, epochs=epochs, batch_size=32)
	makePlots(dataset_name)
	generateImgs(imgs_array, dataset_name, colorMode=colorMode)
	end = time.time()
	elapsedTime_secs = end - start
	elapsedTime_hms = convert_secs_to_hms(elapsedTime_secs)
	print("Benchmark elapsed time:", elapsedTime_hms)

def benchmark_dataset(epochs, path, colorMode, width=None, height=None):
	start = time.time()
	imgs_array = load_dataset(path=path, colorMode=colorMode, width=width, height=height, normalization=1)
	imgs_array2 = load_dataset(path=path, colorMode=colorMode, width=width, height=height, normalization=2)
	trainGAN(imgs_array, dataset_name, epochs=epochs, batch_size=32)
	trainDCGAN(imgs_array, dataset_name, epochs=epochs, batch_size=32)
	trainWGAN(imgs_array, dataset_name, epochs=epochs, batch_size=32)
	#trainAAE(imgs_array, dataset_name, latent_dim=2, epochs=epochs, batch_size=32)
	#trainAAE(imgs_array, dataset_name, latent_dim=10, epochs=epochs, batch_size=32)
	trainVAE(imgs_array2, dataset_name, latent_dim=2, epochs=epochs, batch_size=32)
	trainVAE(imgs_array2, dataset_name, latent_dim=10, epochs=epochs, batch_size=32)
	makePlots(dataset_name)
	generateImgs(imgs_array, dataset_name, colorMode=colorMode)
	end = time.time()
	elapsedTime_secs = end - start
	elapsedTime_hms = convert_secs_to_hms(elapsedTime_secs)
	print("Benchmark elapsed time:", elapsedTime_hms)

if __name__ == '__main__':
	"""
	imgs_array = load_tf_dataset(datasetNb=2, colorMode="gray")
	dataset_name = "Fashion_MNIST"
	#trainGAN(imgs_array, dataset_name, epochs=2, batch_size=32)
	#trainDCGAN(imgs_array, dataset_name, epochs=2, batch_size=32)
	#trainWGAN(imgs_array, dataset_name, epochs=2, batch_size=32)
	trainAAE(imgs_array, dataset_name, latent_dim=2, epochs=2, batch_size=32)
	#trainAAE(imgs_array, dataset_name, latent_dim=10, epochs=2, batch_size=32)
	imgs_array = load_tf_dataset(datasetNb=2, colorMode="gray", normalization=2)
	#trainVAE(imgs_array, dataset_name, latent_dim=2, epochs=2, batch_size=32)
	#trainVAE(imgs_array, dataset_name, latent_dim=10, epochs=2, batch_size=32)
	#makePlots(dataset_name)
	#generateImgs(imgs_array, dataset_name, colorMode="gray")
	"""
	#benchmark_tf_dataset(epochs=20, datasetNb=1, colorMode="gray", width=None, height=None)	
	benchmark_tf_dataset(epochs=20, datasetNb=2, colorMode="gray", width=None, height=None)	
	#benchmark_tf_dataset(epochs=2, datasetNb=3, colorMode="color", width=28, height=28)
	#benchmark_tf_dataset(epochs=2, datasetNb=3, colorMode="color", width=28, height=28, classNbs=[3])

	"""
	datasetsPath = "../Datasets/"
	dataset_name = "anime_faces_dataset_28x28"
	path = datasetsPath+dataset_name+"/"
	benchmark_dataset(epochs=20, path=path, colorMode="color")
	"""