"""
We adapted the code from the following website: 
https://github.com/eriklindernoren/Keras-GAN/blob/master/wgan/wgan.py
in order to evaluate the GAN.

Modifications:
Update imports to make it run with tensorflow.
Some changes in order to save the results for the evaluation part.
We added code to load CIFAR dataset.

GAN description:
The GAN from this file is a GAN containing convolutional layers and using a
loss function called wassertein loss. 
This GAN is called Wassertein GAN (WGAN).
"""

from tensorflow.keras.datasets import cifar10
from tensorflow.keras.datasets import mnist
from tensorflow.keras.layers import Input, Dense, Reshape, Flatten, Dropout
from tensorflow.keras.layers import BatchNormalization, Activation, ZeroPadding2D
from tensorflow.keras.layers import LeakyReLU
from tensorflow.keras.layers import UpSampling2D, Conv2D
from tensorflow.keras.models import Sequential, Model
from tensorflow.keras.optimizers import RMSprop

import keras.backend as K

import matplotlib.pyplot as plt
import numpy as np
import csv
import time

from utils import *

generativeModelName = "WGAN" 
trainingGeneratedImagesPath = "./Training_Generated_Images/"+generativeModelName+"/"
generatedImagesPath = "./Generated_Images/"+generativeModelName+"/"
resultsPath = "./Results/"
resultsCsvFilename = "results_"+generativeModelName+".csv"
modelsPath = "./Models/"

class WGAN():
	def __init__(self):
		self.img_rows = 28
		self.img_cols = 28
		self.channels = 3
		self.img_shape = (self.img_rows, self.img_cols, self.channels)
		self.latent_dim = 100
		# Following parameter and optimizer set as recommended in paper
		self.n_critic = 5
		self.clip_value = 0.01
		optimizer = RMSprop(lr=0.00005)
		# Build and compile the critic
		self.critic = self.build_critic()
		self.critic.compile(loss=self.wasserstein_loss, optimizer=optimizer, metrics=['accuracy'])
		# Build the generator
		self.generator = self.build_generator()
		# The generator takes noise as input and generated imgs
		z = Input(shape=(self.latent_dim,))
		img = self.generator(z)
		# For the combined model we will only train the generator
		self.critic.trainable = False
		# The critic takes generated images as input and determines validity
		valid = self.critic(img)
		# The combined model  (stacked generator and critic)
		self.combined = Model(z, valid)
		self.combined.compile(loss=self.wasserstein_loss, optimizer=optimizer, metrics=['accuracy'])

	def wasserstein_loss(self, y_true, y_pred):
		return K.mean(y_true * y_pred)

	def build_generator(self):
		model = Sequential(name="Generator")
		model.add(Dense(128 * 7 * 7, activation="relu", input_dim=self.latent_dim))
		model.add(Reshape((7, 7, 128)))
		model.add(UpSampling2D())
		model.add(Conv2D(128, kernel_size=4, padding="same"))
		model.add(BatchNormalization(momentum=0.8))
		model.add(Activation("relu"))
		model.add(UpSampling2D())
		model.add(Conv2D(64, kernel_size=4, padding="same"))
		model.add(BatchNormalization(momentum=0.8))
		model.add(Activation("relu"))
		model.add(Conv2D(self.channels, kernel_size=4, padding="same"))
		model.add(Activation("tanh"))
		model.summary()
		noise = Input(shape=(self.latent_dim,))
		img = model(noise)
		return Model(noise, img)

	def build_critic(self):
		model = Sequential(name="Discriminator")
		model.add(Conv2D(16, kernel_size=3, strides=2, input_shape=self.img_shape, padding="same"))
		model.add(LeakyReLU(alpha=0.2))
		model.add(Dropout(0.25))
		model.add(Conv2D(32, kernel_size=3, strides=2, padding="same"))
		model.add(ZeroPadding2D(padding=((0,1),(0,1))))
		model.add(BatchNormalization(momentum=0.8))
		model.add(LeakyReLU(alpha=0.2))
		model.add(Dropout(0.25))
		model.add(Conv2D(64, kernel_size=3, strides=2, padding="same"))
		model.add(BatchNormalization(momentum=0.8))
		model.add(LeakyReLU(alpha=0.2))
		model.add(Dropout(0.25))
		model.add(Conv2D(128, kernel_size=3, strides=1, padding="same"))
		model.add(BatchNormalization(momentum=0.8))
		model.add(LeakyReLU(alpha=0.2))
		model.add(Dropout(0.25))
		model.add(Flatten())
		model.add(Dense(1))
		model.summary()
		img = Input(shape=self.img_shape)
		validity = model(img)
		return Model(img, validity)

	def load_MNIST(self):
		(X_train, _), (_, _) = mnist.load_data()
		X_train = convertImgsArray(X_train, colorMode="color")
		X_train = (X_train.astype('float32') - 127.5) / 127.5  # Normalize the images to [-1, 1]
		return X_train

	def load_CIFAR(self):
		# cifar_classes = ["airplane", "automobile", "bird", "cat", "deer", "dog", "frog", "horse", "ship", "truck"]
		(X_train, _), (_, _) = cifar10.load_data()
		# Resize the colored images 32x32 to 28x28 and convert them to grayscale.
		X_train = resizeImgsArray(X_train, width=28, height=28, colorMode="color")
		X_train = (X_train.astype('float32') - 127.5) / 127.5  # Normalize the images to [-1, 1]
		return X_train

	def train(self, epochs, batch_size=128, sample_interval=50):
		self.initialize_directories()
		# Load the dataset
		#X_train = self.load_MNIST()
		X_train = self.load_CIFAR()
		# Adversarial ground truths
		valid = -np.ones((batch_size, 1))
		fake = np.ones((batch_size, 1))
		for epoch in range(epochs):
			for _ in range(self.n_critic):
				# Train Discriminator
				# Select a random batch of images
				idx = np.random.randint(0, X_train.shape[0], batch_size)
				imgs = X_train[idx]
				# Sample noise as generator input
				noise = np.random.normal(0, 1, (batch_size, self.latent_dim))
				# Generate a batch of new images
				gen_imgs = self.generator.predict(noise)
				# Train the critic
				d_loss_real, d_accuracy_real = self.critic.train_on_batch(imgs, valid)
				d_loss_fake, d_accuracy_fake = self.critic.train_on_batch(gen_imgs, fake)
				d_loss = 0.5 * np.add(d_loss_real, d_loss_fake)
				d_accuracy = 0.5 * np.add(d_accuracy_real, d_accuracy_fake)
				# Clip critic weights
				for l in self.critic.layers:
					weights = l.get_weights()
					weights = [np.clip(w, -self.clip_value, self.clip_value) for w in weights]
					l.set_weights(weights)
			# Train Generator
			g_loss, g_accuracy = self.combined.train_on_batch(noise, valid)
			# About wasserstein-loss
			# https://machinelearningmastery.com/how-to-implement-wasserstein-loss-for-generative-adversarial-networks/
			# https://neptune.ai/blog/gan-loss-functions
			# Plot the progress
			#print ("%d [D loss: %f] [G loss: %f]" % (epoch, 1 - d_loss[0], 1 - g_loss[0]))
			d_loss = 1 - d_loss
			g_loss = 1 - g_loss
			print ("Epoch %d [D loss: %f, D acc: %f%%, D acc real %f%%, D acc fake %f%%] [G loss: %f]" \
				% (epoch+1, d_loss, 100*d_accuracy, 100*d_accuracy_real, 100*d_accuracy_fake, g_loss))
			self.save_results(epoch+1, [d_loss, 100*d_accuracy, 100*d_accuracy_real, 100*d_accuracy_fake, g_loss], sample_interval)
		self.save_generator()
		#imgsPIL = loadImgsPIL(trainingGeneratedImagesPath)
		#self.generate_gif(imgsPIL, trainingGeneratedImagesPath)
		#print("Metrics D", self.critic.metrics_names) # ['loss', 'accuracy']
		#print("Metrics G", self.combined.metrics_names) # ['loss', 'accuracy']

	def save_results(self, epoch, resultsList, sample_interval):
		loss_interval = 1
		if epoch % loss_interval == 0:
			with open(resultsPath + resultsCsvFilename, "a", newline='') as csvfile:
				writer = csv.writer(csvfile, delimiter=';')
				writer.writerow([epoch]+resultsList)
		if epoch % sample_interval == 0: # Save generated image samples
			self.sample_images(epoch)

	def sample_images(self, epoch):
		r, c = 5, 5
		noise = np.random.normal(0, 1, (r * c, self.latent_dim))
		gen_imgs = self.generator.predict(noise)
		# Rescale images 0 - 1
		gen_imgs = 0.5 * gen_imgs + 0.5
		fig, axs = plt.subplots(r, c)
		cnt = 0
		for i in range(r):
			for j in range(c):
				imgs = gen_imgs[cnt] = (gen_imgs[cnt]) * 127.5 + 127.5
				imgs = imgs.astype(int)
				axs[i,j].imshow(imgs)
				axs[i,j].axis('off')
				cnt += 1
		fig.suptitle("Epoch " + str(epoch))
		fig.savefig(trainingGeneratedImagesPath+"%d.png" % epoch)
		plt.close()

	def generate_imgs(self, imgsNb=10):
		noise = np.random.normal(0, 1, (imgsNb, self.latent_dim))
		print(noise.shape)
		gen_imgs = self.generator.predict(noise)
		gen_imgs = (gen_imgs) * 127.5 + 127.5
		gen_imgs = gen_imgs.astype(np.uint8)
		gen_imgs = np.squeeze(gen_imgs)
		return gen_imgs

	def generate_imgs_to(self, path, imgsNb=10):
		genImgs = self.generate_imgs(imgsNb)
		genImgsPIL = convertToImgPIL(genImgs)
		emptyDirectory(generatedImagesPath)
		createDirectory(generatedImagesPath)
		saveImgsPIL(genImgsPIL, generatedImagesPath)
		#self.generate_gif(genImgsPIL, path)

	def generate_gif(self, imgsPIL, path):
		generateGif(imgsPIL, path, filename=generativeModelName)

	def initialize_directories(self):
		createDirectory(trainingGeneratedImagesPath)
		createDirectory(resultsPath)
		removeFile(resultsPath + resultsCsvFilename)
		emptyDirectory(trainingGeneratedImagesPath)

	def save_generator(self):
		createDirectory(modelsPath)
		self.generator.save(modelsPath+generativeModelName+"_generator.h5")

if __name__ == '__main__':
	wgan = WGAN()
	f = open("training_time.txt", "a")
	start = time.time()
	epochs = 10000
	batch_size = 32
	wgan.train(epochs=epochs, batch_size=batch_size, sample_interval=100)
	end = time.time()
	elapsedTime_secs = end - start
	elapsedTime_hms = convert_secs_to_hms(elapsedTime_secs)
	print("Elapsed time:", elapsedTime_hms)
	f.write(generativeModelName+", Epochs "+str(epochs)+", Batch Size "+str(batch_size)+", Training Time "+elapsedTime_hms+"\n")
	f.close()
	wgan.generate_imgs_to(generatedImagesPath, 100)