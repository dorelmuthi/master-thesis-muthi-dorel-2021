import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns; sns.set()

from utils import *

def getCsvResultPathOf(generativeModelName):
	resultsPath = "./Results/"
	resultsCsvFilename = "results_"+generativeModelName+".csv"
	return resultsPath + resultsCsvFilename

def getDataFrameFromCsv(path):
	col_names = ['epoch', 'd_loss', 'acc', 'acc_real', 'acc_fake', 'g_loss']
	dataFrame = pd.read_csv(path, delimiter=';', names=col_names)
	#df = pd.read_csv(resultPathAAE, delimiter=';', names=col_names, header=1) # header=1 to ignore header on the first line of csv
	return dataFrame

def plotLoss(plotsPath, generativeModelName, dataFrame):
	fig = plt.figure(figsize=(16, 8), frameon=False)
	fig.set_size_inches(16, 8)
	sns.lineplot(data=dataFrame, x='epoch', y='d_loss', label='d_loss')
	s = sns.lineplot(data=dataFrame, x='epoch', y='g_loss', label='g_loss')
	s.set_ylabel('Loss')
	s.set_xlabel('Epoch')
	plt.savefig(plotsPath + generativeModelName +'_loss.png', bbox_inches='tight', pad_inches=0)
	#plt.show()

def plotAcc(plotsPath, generativeModelName, dataFrame):
	fig = plt.figure(figsize=(16, 8))
	sns.lineplot(data=dataFrame, x='epoch', y='acc', label='acc')
	sns.lineplot(data=dataFrame, x='epoch', y='acc_real', label='acc_real')
	s = sns.lineplot(data=dataFrame, x='epoch', y='acc_fake', label='acc_fake')
	s.set_ylabel('Accuracy (%)')
	s.set_xlabel('Epoch')
	plt.savefig(plotsPath + generativeModelName +'_acc.png', bbox_inches='tight', pad_inches=0)
	#plt.show()

def plotLossVAE(plotsPath, latent_dim):
	resultsPath = "./Results/"
	resultsCsvFilename = "results_VAE_latent_dim_"+str(latent_dim)+".csv"
	path = resultsPath + resultsCsvFilename
	col_names = ['epoch', 'loss']
	dataFrame = pd.read_csv(path, delimiter=';', names=col_names)
	fig = plt.figure(figsize=(16, 8), frameon=False)
	fig.set_size_inches(16, 8)
	s = sns.lineplot(data=dataFrame, x='epoch', y='loss', label='loss')
	s.set_ylabel('Loss')
	s.set_xlabel('Epoch')
	plt.savefig(plotsPath + "VAE_latent_dim_"+str(latent_dim)+"_loss.png", bbox_inches='tight', pad_inches=0)
	#plt.show()
	
# With moving average (also called rolling average)
def plotAccMovingAvg(plotsPath, generativeModelName, dataFrame, winSize=100):
	dataFrame[ 'acc_rolling_avg' ] = dataFrame.acc.rolling(winSize).mean()
	dataFrame[ 'acc_real_rolling_avg' ] = dataFrame.acc_real.rolling(winSize).mean()
	dataFrame[ 'acc_fake_rolling_avg' ] = dataFrame.acc_fake.rolling(winSize).mean()
	plt.figure(figsize=(16, 8))
	sns.lineplot(data=dataFrame, x="epoch", y="acc_rolling_avg", label='acc')
	sns.lineplot(data=dataFrame, x="epoch", y="acc_real_rolling_avg", label='acc_real')
	s = sns.lineplot(data=dataFrame, x="epoch", y="acc_fake_rolling_avg", label='acc_fake')
	s.set_ylabel('Accuracy (%)')
	s.set_xlabel('Epoch')
	plt.savefig(plotsPath + generativeModelName +'_accMovingAvg.png', bbox_inches='tight', pad_inches=0)
	#plt.show()

if __name__ == '__main__':
	sns.set(font_scale=1.5)
	p = sns.color_palette("bright") # (deep, muted, bright, pastel, dark, colorblind)
	sns.palplot(p)
	sns.set_palette(p)
	cmap = plt.cm.get_cmap('Spectral') # https://matplotlib.org/3.5.0/gallery/color/colormap_reference.html
	sns.set_context("notebook", font_scale=1.5, rc={"lines.linewidth": 3})

	plotsPath = "./Plots/"
	#emptyDirectory(plotsPath)
	createDirectory(plotsPath)

	dataFrameAAE = getDataFrameFromCsv("./Results/results_AAE_latent_dim_2.csv")
	plotLoss(plotsPath, "AAE_latent_dim_2", dataFrameAAE)
	plotAcc(plotsPath, "AAE_latent_dim_2", dataFrameAAE)
	plotAccMovingAvg(plotsPath, "AAE_latent_dim_2", dataFrameAAE, winSize=100)

	dataFrameAAE = getDataFrameFromCsv("./Results/results_AAE_latent_dim_10.csv")
	plotLoss(plotsPath, "AAE_latent_dim_10", dataFrameAAE)
	plotAcc(plotsPath, "AAE_latent_dim_10", dataFrameAAE)
	plotAccMovingAvg(plotsPath, "AAE_latent_dim_10", dataFrameAAE, winSize=100)

	resultPathDCGAN = getCsvResultPathOf("DCGAN")
	dataFrameDCGAN = getDataFrameFromCsv(resultPathDCGAN)
	plotLoss(plotsPath, "DCGAN", dataFrameDCGAN)
	plotAcc(plotsPath, "DCGAN", dataFrameDCGAN)
	plotAccMovingAvg(plotsPath, "DCGAN", dataFrameDCGAN, winSize=100)

	resultPathGAN = getCsvResultPathOf("GAN")
	dataFrameGAN = getDataFrameFromCsv(resultPathGAN)
	plotLoss(plotsPath, "GAN", dataFrameGAN)
	plotAcc(plotsPath, "GAN", dataFrameGAN)
	plotAccMovingAvg(plotsPath, "GAN", dataFrameGAN, winSize=100)

	resultPath = getCsvResultPathOf("WGAN")
	dataFrame = getDataFrameFromCsv(resultPath)
	plotLoss(plotsPath, "WGAN", dataFrame)

	plotLossVAE(plotsPath, latent_dim=2)
	plotLossVAE(plotsPath, latent_dim=10)