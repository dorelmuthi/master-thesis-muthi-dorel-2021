from utils import *

if __name__ == '__main__':
	gifsPath = "./Gifs/"
	createDirectory(gifsPath)
	trainingGeneratedImagesPath = "./Training_Generated_Images/"
	genModelName = "GAN"
	imgs = loadImgsPIL(trainingGeneratedImagesPath+genModelName+"/")
	generateGif(imgs, gifsPath, genModelName)

	genModelName = "DCGAN"
	imgs = loadImgsPIL(trainingGeneratedImagesPath+genModelName+"/")
	generateGif(imgs, gifsPath, genModelName)

	genModelName = "WGAN"
	imgs = loadImgsPIL(trainingGeneratedImagesPath+genModelName+"/")
	generateGif(imgs, gifsPath, genModelName)

	genModelName = "AAE"
	latentDim = 2
	imgs = loadImgsPIL(trainingGeneratedImagesPath+genModelName+"/latent_dim_"+str(latentDim)+"/")
	generateGif(imgs, gifsPath, genModelName+"_latent_dim_"+str(latentDim))

	genModelName = "AAE"
	latentDim = 10
	imgs = loadImgsPIL(trainingGeneratedImagesPath+genModelName+"/latent_dim_"+str(latentDim)+"/")
	generateGif(imgs, gifsPath, genModelName+"_latent_dim_"+str(latentDim))