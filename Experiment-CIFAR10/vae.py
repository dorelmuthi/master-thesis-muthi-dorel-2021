"""
We adapted the code from the following website: 
https://www.geeksforgeeks.org/variational-autoencoders/
in order to evaluate the VAE

Modifications:
Some changes in order to save the results for the evaluation part.
We updated the code for CIFAR-10 dataset.

VAE description:
Variational Auto-encoder is a variant of Auto-encoder network.
This variant is a generative network.
"""

import numpy as np
import tensorflow as tf
from tensorflow.keras.datasets import cifar10
from tensorflow import keras
from tensorflow.keras import Input, Model
from tensorflow.keras.layers import Layer, Conv2D, Flatten, Dense, Reshape, Conv2DTranspose
import matplotlib.pyplot as plt
import time
from utils import *

generativeModelName = "VAE" 
trainingGeneratedImagesPath = "./Training_Generated_Images/"+generativeModelName+"/"
generatedImagesPath = "./Generated_Images/"+generativeModelName+"/"
resultsPath = "./Results/"
resultsCsvFilename = "results_"+generativeModelName
plotsPath = "./Plots/"
modelsPath = "./Models/"

# this sampling layer is the bottleneck layer of variational autoencoder,
# it uses the output from two dense layers z_mean and z_log_var as input,
# convert them into normal distribution and pass them to the decoder layer
class Sampling(Layer):
	def call(self, inputs):
		z_mean, z_log_var = inputs
		batch = tf.shape(z_mean)[0]
		dim = tf.shape(z_mean)[1]
		epsilon = tf.keras.backend.random_normal(shape =(batch, dim))
		return z_mean + tf.exp(0.5 * z_log_var) * epsilon

# define the complete variational autoencoder architecture
class VAE(keras.Model):
	def __init__(self, latent_dim=2, **kwargs):
		super(VAE, self).__init__(**kwargs)
		self.latent_dim = latent_dim
		self.encoder = self.build_encoder()
		self.decoder = self.build_decoder()
 
	def build_encoder(self):
		encoder_inputs = Input(shape =(28, 28, 3))
		x = Conv2D(32, 3, activation ="relu", strides = 2, padding ="same")(encoder_inputs)
		x = Conv2D(64, 3, activation ="relu", strides = 2, padding ="same")(x)
		x = Flatten()(x)
		x = Dense(16, activation ="relu")(x)
		z_mean = Dense(self.latent_dim, name ="z_mean")(x)
		z_log_var = Dense(self.latent_dim, name ="z_log_var")(x)
		z = Sampling()([z_mean, z_log_var])
		encoder = Model(encoder_inputs, [z_mean, z_log_var, z], name ="encoder")
		encoder.summary()
		return encoder

	def build_decoder(self):
		# Define Decoder Architecture
		latent_inputs = keras.Input(shape =(self.latent_dim, ))
		x = Dense(7 * 7 * 64, activation ="relu")(latent_inputs)
		x = Reshape((7, 7, 64))(x)
		x = Conv2DTranspose(64, 3, activation ="relu", strides = 2, padding ="same")(x)
		x = Conv2DTranspose(32, 3, activation ="relu", strides = 2, padding ="same")(x)
		decoder_outputs = Conv2DTranspose(3, 3, activation ="sigmoid", padding ="same")(x)
		decoder = Model(latent_inputs, decoder_outputs, name ="decoder")
		decoder.summary()
		return decoder

	def train_step(self, data):
		if isinstance(data, tuple):
			data = data[0]
		with tf.GradientTape() as tape:
			z_mean, z_log_var, z = self.encoder(data)
			reconstruction = self.decoder(z)
			reconstruction_loss = tf.reduce_mean(keras.losses.binary_crossentropy(data, reconstruction))
			reconstruction_loss *= 28 * 28
			kl_loss = 1 + z_log_var - tf.square(z_mean) - tf.exp(z_log_var)
			kl_loss = tf.reduce_mean(kl_loss)
			kl_loss *= -0.5
			total_loss = reconstruction_loss + kl_loss
		grads = tape.gradient(total_loss, self.trainable_weights)
		self.optimizer.apply_gradients(zip(grads, self.trainable_weights))
		return {"loss": total_loss,"reconstruction_loss": reconstruction_loss, "kl_loss": kl_loss,}

	def train(self, epochs=10, batch_size=32):
		self.initialize_directories()
		# cifar_classes = ["airplane", "automobile", "bird", "cat", "deer", "dog", "frog", "horse", "ship", "truck"]
		(x_train, y_train), (x_test, y_test) = cifar10.load_data()
		# Resize the colored images 32x32 to 28x28.
		x_train = resizeImgsArray(x_train, width=28, height=28, colorMode="color")
		x_train = x_train.astype("float32") / 255 # Normalize the images to [0, 1]
		# compile and train the model
		self.compile(optimizer ='rmsprop')
		history = self.fit(x_train, epochs = epochs, batch_size = batch_size, verbose=2)
		#print("history keys",history.history.keys())
		# save loss
		import csv
		csvfile = open(resultsPath + "results_" + generativeModelName +"_latent_dim_"+str(self.latent_dim)+".csv", 'w', newline='')
		writer = csv.writer(csvfile, delimiter=';')
		for epoch, loss in enumerate(history.history['loss']):
			writer.writerow([epoch+1, loss])
		csvfile.close()
		# VAE plots if latent_dim=2
		if self.latent_dim==2:
			self.plot_latent()
			self.plot_label_clusters(x_train, y_train)
		self.save_decoder()  
	
	def plot_latent(self):
		# display a n * n 2D manifold of images
		n = 10
		img_dim = 28
		img_channel = 3
		scale = 2.0
		figsize = 15
		figure = np.zeros((img_dim * n, img_dim * n, img_channel))
		# linearly spaced coordinates corresponding to the 2D plot
		# of img classes in the latent space
		grid_x = np.linspace(-scale, scale, n)
		grid_y = np.linspace(-scale, scale, n)[::-1]
		for i, yi in enumerate(grid_y):
			for j, xi in enumerate(grid_x):
				z_sample = np.array([[xi, yi]])
				x_decoded = self.decoder.predict(z_sample)
				x_decoded = x_decoded * 255
				x_decoded = x_decoded.astype(np.uint8)
				img = x_decoded[0].reshape(img_dim, img_dim, img_channel)
				figure[
					i * img_dim : (i + 1) * img_dim,
					j * img_dim : (j + 1) * img_dim,
				] = img
		plt.figure(figsize=(figsize, figsize))
		start_range = img_dim // 2
		end_range = n * img_dim + start_range
		pixel_range = np.arange(start_range, end_range, img_dim)
		sample_range_x = np.round(grid_x, 1)
		sample_range_y = np.round(grid_y, 1)
		plt.xticks(pixel_range, sample_range_x)
		plt.yticks(pixel_range, sample_range_y)
		plt.xlabel("z[0]")
		plt.ylabel("z[1]")
		figure = figure.astype(np.uint8)
		plt.imshow(figure)
		plt.savefig(plotsPath + generativeModelName +'_latent.png', bbox_inches='tight', pad_inches=0)
		#plt.show()

	def plot_label_clusters(self, data, test_lab):
		cifar_classes = ["airplane", "car", "bird", "cat", "deer", "dog", "frog", "horse", "ship", "truck"]
		labels = {i : cifar_classes[i] for i in range(len(cifar_classes))}
		z_mean, _, _ = self.encoder.predict(data)
		plt.figure(figsize =(12, 10))
		sc = plt.scatter(z_mean[:, 0], z_mean[:, 1], c = test_lab)
		cbar = plt.colorbar(sc, ticks = range(10))
		cbar.ax.set_yticklabels([labels.get(i) for i in range(10)])
		plt.xlabel("z[0]")
		plt.ylabel("z[1]")
		plt.savefig(plotsPath + generativeModelName +'_label_clusters.png', bbox_inches='tight', pad_inches=0)
		#plt.show()

	def generate_imgs(self, imgsNb=10):
		gen_imgs = self.decoder.predict(np.random.normal(size=(imgsNb, self.latent_dim)))
		gen_imgs = gen_imgs * 255
		gen_imgs = gen_imgs.astype(np.uint8)
		return gen_imgs

	def generate_imgs_to(self, path, imgsNb=10):
		genImgs = self.generate_imgs(imgsNb)
		genImgsPIL = convertToImgPIL(genImgs)
		emptyDirectory(path)
		createDirectory(path)
		saveImgsPIL(genImgsPIL, path)
		#self.generate_gif(genImgsPIL, path)

	def generate_gif(self, imgsPIL, path):
		generateGif(imgsPIL, path, filename=generativeModelName+"_latent_dim_"+str(self.latent_dim))

	def initialize_directories(self):
		createDirectory(resultsPath)
		removeFile(resultsPath + resultsCsvFilename)
		createDirectory(plotsPath)
		
	def save_decoder(self):
		createDirectory(modelsPath)
		self.decoder.save(modelsPath+generativeModelName+"_latent_dim_"+str(self.latent_dim)+"_decoder.h5")

if __name__ == '__main__':
	latent_dim = 2
	vae = VAE(latent_dim)
	f = open("training_time.txt", "a")
	start = time.time()
	epochs = 100
	batch_size = 32
	vae.train(epochs=epochs, batch_size=batch_size)
	end = time.time()
	elapsedTime_secs = end - start
	elapsedTime_hms = convert_secs_to_hms(elapsedTime_secs)
	print("Elapsed time:", elapsedTime_hms)
	f.write(generativeModelName+", Epochs "+str(epochs)+", Batch Size "+str(batch_size)+", Latent Dim "+str(latent_dim)+", Training Time "+elapsedTime_hms+"\n")
	f.close()
	path = "./Generated_Images/"+generativeModelName+"_latent_dim_"+str(latent_dim)+"/"
	vae.generate_imgs_to(path, 100)

	latent_dim = 10
	vae = VAE(latent_dim)
	f = open("training_time.txt", "a")
	start = time.time()
	epochs = 100
	batch_size = 32
	vae.train(epochs=epochs, batch_size=batch_size)
	end = time.time()
	elapsedTime_secs = end - start
	elapsedTime_hms = convert_secs_to_hms(elapsedTime_secs)
	print("Elapsed time:", elapsedTime_hms)
	f.write(generativeModelName+", Epochs "+str(epochs)+", Batch Size "+str(batch_size)+", Latent Dim "+str(latent_dim)+", Training Time "+elapsedTime_hms+"\n")
	f.close()
	path = "./Generated_Images/"+generativeModelName+"_latent_dim_"+str(latent_dim)+"/"
	vae.generate_imgs_to(path, 100)	