import PIL
from utils import *
import tensorflow as tf
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns; sns.set()

def loadGenImgs(generativeModelName, latent_dim=0):
	if latent_dim == 0:
		generatedImagesPath = "./Generated_Images2/"+generativeModelName+"/"
	else:
		generatedImagesPath = "./Generated_Images2/"+generativeModelName+"_latent_dim_"+str(latent_dim)+"/"
	genImgs = loadImgsArray(generatedImagesPath, colorMode="color", width=32, height=32)
	return genImgs    

def getDataForPlots(genModelName, model, latent_dim=0): # For classification test
	data = loadGenImgs(genModelName, latent_dim) / 255.0
	prediction = np.around(model.predict(data), decimals=1)
	unique, count = np.unique(prediction, return_counts=True)
	prediction_rounded = []
	for element in np.around(model.predict(data)):
		prediction_rounded.append(np.argmax(element))
	hist = np.histogram(prediction_rounded, bins=10)
	return hist, prediction_rounded, unique, count

if __name__ == '__main__':
	sns.set(font_scale=1.5)
	p = sns.color_palette("bright") # (deep, muted, bright, pastel, dark, colorblind)
	sns.palplot(p)
	sns.set_palette(p)
	cmap = plt.cm.get_cmap('Spectral') # https://matplotlib.org/3.5.0/gallery/color/colormap_reference.html
	sns.set_context("notebook", font_scale=1.5, rc={"lines.linewidth": 3})
	plotsPath = "./Plots/"

	# Model source is: https://www.geeksforgeeks.org/cifar-10-image-classification-in-tensorflow/
	cifar_10_classifier = tf.keras.models.load_model('./CIFAR_10_Classifier_Models/CIFAR_10_Classifier2.h5') # load model
	# Model, accuracy: 88.79%

	# Prepare data for plots
	hist_gan, prediction_rounded_gan, unique_gan, count_gan = getDataForPlots("GAN", cifar_10_classifier)
	hist_dcgan, prediction_rounded_dcgan, unique_dcgan, count_dcgan = getDataForPlots("DCGAN", cifar_10_classifier)
	hist_wgan, prediction_rounded_wgan, unique_wgan, count_wgan = getDataForPlots("WGAN", cifar_10_classifier)
	hist_aae, prediction_rounded_aae, unique_aae, count_aae = getDataForPlots("AAE", cifar_10_classifier, latent_dim=10)
	hist_vae, prediction_rounded_vae, unique_vae, count_vae = getDataForPlots("VAE", cifar_10_classifier, latent_dim=10)

	# Plot
	cifar_classes = ["1 airplane", "2 car", "3 bird", "4 cat", "5 deer", "6 dog", "7 frog", "8 horse", "9 ship", "10 truck"]
	x = np.linspace(0,9,10)
	plt.figure(figsize=(16, 10))
	ax1 = plt.subplot(211)
	ax1.bar(x-0.2, hist_gan[0]/len(prediction_rounded_gan), width=0.4, align='center', label='GAN')
	ax1.bar(x-0.1, hist_dcgan[0]/len(prediction_rounded_dcgan), width=0.4, align='center', label='DCGAN')
	ax1.bar(x, hist_wgan[0]/len(prediction_rounded_wgan), width=0.4, align='center', label='WGAN')
	ax1.bar(x+0.1, hist_aae[0]/len(prediction_rounded_aae), width=0.4, align='center', label='AAE')
	ax1.bar(x+0.2, hist_vae[0]/len(prediction_rounded_vae), width=0.4, align='center', label='VAE')
	sns.lineplot(x=[-1, 10], y=[0.1, 0.1], ax=ax1, label='CIFAR-10')  # Baseline
	plt.legend()
	ax1.set_ylabel('Count')
	ax1.set_xlabel('Class')
	plt.xticks(np.arange(0, 10, step=1), labels=cifar_classes)

	ax2 = plt.subplot(223)
	s = sns.lineplot(x=unique_gan, y=count_gan, label='GAN', ax=ax2)
	s = sns.lineplot(x=unique_dcgan, y=count_dcgan, label='DCGAN', ax=ax2)
	s = sns.lineplot(x=unique_wgan, y=count_wgan, label='WGAN', ax=ax2)
	s = sns.lineplot(x=unique_aae, y=count_aae, label='AAE', ax=ax2)
	s = sns.lineplot(x=unique_vae, y=count_vae, label='VAE', ax=ax2)
	s.set_ylabel('Count')
	s.set_xlabel('Confidence probability')
	plt.xticks(np.arange(0, 1.1, step=0.1))

	ax2 = plt.subplot(224)
	s = sns.lineplot(x=unique_gan, y=count_gan, label='GAN', ax=ax2)
	s = sns.lineplot(x=unique_dcgan, y=count_dcgan, label='DCGAN', ax=ax2)
	s = sns.lineplot(x=unique_wgan, y=count_wgan, label='WGAN', ax=ax2)
	s = sns.lineplot(x=unique_aae, y=count_aae, label='AAE', ax=ax2)
	s = sns.lineplot(x=unique_vae, y=count_vae, label='VAE', ax=ax2)
	s.semilogy()
	s.set_ylabel('Count')
	s.set_xlabel('Confidence probability')
	plt.xticks(np.arange(0, 1.1, step=0.1))
	plt.savefig(plotsPath + 'classification_test.png', bbox_inches='tight', pad_inches=0)
	#plt.show()