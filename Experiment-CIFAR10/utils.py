import os
import glob
import PIL
import PIL.Image
import numpy as np

# Functions for files and folder
# ------------------------------
def createDirectory(path):
	if not os.path.isdir(path):
		os.makedirs(path)

def removeFile(path):
	if os.path.isfile(path):
		os.remove(path)

def emptyDirectory(path):
	if os.path.isdir(path):
		files = glob.glob(path+"*")
		for file in files:
			os.remove(file)
#-------------------------------

# Functions for images
# ------------------------------
def convertColorMode(imgPIL, colorMode):
	if colorMode == "color":
		imgPIL = imgPIL.convert('RGB')
	elif colorMode == "gray":
		imgPIL = imgPIL.convert('L')
	return imgPIL

def resizeImgsArray(imgsArray, width, height, colorMode="color"):
	tmp = []
	for imgArray in imgsArray:
		imgPIL = PIL.Image.fromarray(imgArray)
		imgPIL = imgPIL.resize((width, height))
		imgPIL = convertColorMode(imgPIL, colorMode)
		array = np.asarray(imgPIL)
		tmp.append(array)
		imgPIL.close()
	imgsArray = np.asarray(tmp)
	return imgsArray

# Used to change the color mode
def convertImgsArray(imgsArray ,colorMode):
	tmp = []
	for imgArray in imgsArray:
		imgPIL = PIL.Image.fromarray(imgArray)
		imgPIL = convertColorMode(imgPIL, colorMode)
		array = np.asarray(imgPIL)
		tmp.append(array)
	imgsArray = np.asarray(tmp)
	return imgsArray

def convertToImgPIL(imgsArray, colorMode="color"):
	imgsPIL = []
	for imgArray in imgsArray:
		imgPIL = PIL.Image.fromarray(imgArray)
		imgPIL = convertColorMode(imgPIL, colorMode)
		imgsPIL.append(imgPIL)
	return imgsPIL

def convertToImgsArray(imgsPIL):
	imgsArray = []
	for imgPIL in imgsPIL:
		imgArray = np.asarray(imgPIL)
		imgsArray.append(imgArray)
	imgsArray = np.asarray(imgsArray)
	return imgsArray

def saveImgsPIL(imgsPIL, path, colorMode="color"):
	for idx, imgPIL in enumerate(imgsPIL):
		imgPIL = convertColorMode(imgPIL, colorMode)
		imgPIL.save(path+str(idx+1)+".png")

def loadImgsPIL(path, colorMode="color"):
	imgsPath = glob.glob(path+"*.png")
	# Sort the paths by the number of the image.
	tmp = []
	for imgPath in imgsPath:
		imgNb = os.path.split(imgPath)[-1][:-4]
		tmp.append([int(imgNb), imgPath])
	tmp.sort(key = lambda x: x[0]) # sort by first element the lists of tmp
	imgsPath = [elem[1] for elem in tmp]
	imgsPIL = []
	for imgPath in imgsPath:
		imgPIL = PIL.Image.open(imgPath)
		imgsPIL.append(imgPIL)
	return imgsPIL

def loadImgsArray(path, colorMode="color", width=32, height=32):
	imgsPath = glob.glob(path+"*.png")
	# Sort the paths by the number of the image.
	tmp = []
	for imgPath in imgsPath:
		imgNb = os.path.split(imgPath)[-1][:-4]
		tmp.append([int(imgNb), imgPath])
	tmp.sort(key = lambda x: x[0]) # sort by first element the lists of tmp
	imgsPath = [elem[1] for elem in tmp]
	imgsArray = []
	for imgPath in imgsPath:
		imgPIL = PIL.Image.open(imgPath)
		imgArray = convertToImgsArray([imgPIL])[0]
		imgArray = resizeImgsArray([imgArray], width, height)[0]
		imgsArray.append(imgArray)
		imgPIL.close()
	imgsArray = np.asarray(imgsArray)
	return imgsArray

def generateGif(imgsPIL, path, filename):
	imgsPIL[0].save(path+filename+".gif", save_all=True, append_images=imgsPIL[1:], optimize=False, duration=150, loop=0)
#-------------------------------

# Other functions
# ------------------------------
def convert_secs_to_hms(seconds):
    minutes = seconds // 60
    hours = minutes // 60
    return ("%02d:%02d:%02d" % (hours, minutes % 60, seconds % 60))