# Code based on:
# https://machinelearningmastery.com/how-to-implement-the-frechet-inception-distance-fid-from-scratch/
# Frechet Inception Distance (FID)
import numpy as np
from scipy.linalg import sqrtm
import tensorflow as tf
from tensorflow.keras.applications.inception_v3 import InceptionV3
from tensorflow.keras.applications.inception_v3 import preprocess_input
from tensorflow.keras.datasets.mnist import load_data
from tensorflow.keras.datasets import cifar10
import csv
from utils import *
 
def getFidScore(generativeModelName, latent_dim=0): # Computes frechet inception distance
    # Generated Images
    if latent_dim==0:
        generatedImagesPath = "./Generated_Images2/"+generativeModelName+"/"
    else:
        generatedImagesPath = "./Generated_Images2/"+generativeModelName+"_latent_dim_"+str(latent_dim)+"/"
    genImgsPIL = loadImgsPIL(generatedImagesPath)
    genImgs = convertToImgsArray(genImgsPIL)
    images = genImgs.astype('float32') # convert from uint8 to float32
    # Real images
    (x_train, y_train), (x_test, y_test) = cifar10.load_data()
    np.random.shuffle(x_train)
    realImgs = x_train[:len(genImgs)]
    # Resize Images
    realImgs = resizeImgsArray(realImgs, width=299, height=299)
    genImgs = resizeImgsArray(genImgs, width=299, height=299)
    # Convert from uint8 to float32
    realImgs = realImgs.astype('float32')
    genImgs = genImgs.astype('float32')
    model = InceptionV3(include_top=False, pooling='avg', input_shape=(299,299,3))
    # Pre-process images
    realImgs = preprocess_input(realImgs)
    genImgs = preprocess_input(genImgs)
    # calculate activations
    act1 = model.predict(realImgs)
    act2 = model.predict(genImgs)
    # calculate mean and covariance statistics
    mu1, sigma1 = act1.mean(axis=0), np.cov(act1, rowvar=False)
    mu2, sigma2 = act2.mean(axis=0), np.cov(act2, rowvar=False)
    # calculate sum squared difference between means
    ssdiff = np.sum((mu1 - mu2)**2.0)
    # calculate sqrt of product between cov
    covmean = sqrtm(sigma1.dot(sigma2))
    # check and correct imaginary numbers from sqrt
    if np.iscomplexobj(covmean):
        covmean = covmean.real
    # calculate score
    fid = ssdiff + np.trace(sigma1 + sigma2 - 2.0 * covmean)
    tf.keras.backend.clear_session()
    return fid
if __name__ == '__main__':
    resultsPath="./Results/"
    createDirectory(resultsPath)
    f = open(resultsPath+'FID_Score.csv', 'w',  newline='')
    writer = csv.writer(f, delimiter=';')
    writer.writerow(["Generative Model", "FID"])
    fid = getFidScore(generativeModelName="GAN")
    print('GAN FID: %.3f' % fid)
    writer.writerow(["GAN", fid])
    fid = getFidScore(generativeModelName="DCGAN")
    print('DCGAN FID: %.3f' % fid)
    writer.writerow(["DCGAN", fid])
    fid = getFidScore(generativeModelName="WGAN")
    print('WGAN FID: %.3f' % fid)
    writer.writerow(["WGAN", fid])
    fid = getFidScore(generativeModelName="AAE", latent_dim=10)
    print('AAE FID: %.3f' % fid)
    writer.writerow(["AAE", fid])
    fid = getFidScore(generativeModelName="VAE", latent_dim=10)
    print('VAE FID: %.3f' % fid)
    writer.writerow(["VAE", fid])
    f.close()