"""
We adapted the code from the following website: 
https://github.com/eriklindernoren/Keras-GAN/blob/master/aae/aae.py
in order to evaluate the GAN.

Modifications:
Update imports to make it run with tensorflow.
Some changes in order to save the results for the evaluation part.
We added code to load CIFAR dataset.

AAE description:
Adversarial Auto-Encoder (AAE) is a generative model 
trained using an auto-encoder and a discriminator. 
"""

from tensorflow.keras.datasets import cifar10
from tensorflow.keras.datasets import mnist
from tensorflow.keras.layers import Input, Dense, Reshape, Flatten, Dropout, multiply, GaussianNoise
from tensorflow.keras.layers import BatchNormalization, Activation, Embedding, ZeroPadding2D
from tensorflow.keras.layers import MaxPooling2D, Lambda # , merge
from tensorflow.keras.layers import LeakyReLU
from tensorflow.keras.layers import UpSampling2D, Conv2D
from tensorflow.keras.models import Sequential, Model
from tensorflow.keras.optimizers import Adam
from tensorflow.keras import losses
from tensorflow.keras.utils import to_categorical
import keras.backend as K

import matplotlib.pyplot as plt
import numpy as np
import csv
import time

from utils import *

generativeModelName = "AAE" 
trainingGeneratedImagesPath = "./Training_Generated_Images/"+generativeModelName+"/"
generatedImagesPath = "./Generated_Images/"+generativeModelName+"/"
resultsPath = "./Results/"
resultsCsvFilename = "results_"+generativeModelName+".csv"
plotsPath = "./Plots/"
modelsPath = "./Models/"

class AdversarialAutoencoder():
	def __init__(self, latent_dim=2):
		self.img_rows = 28
		self.img_cols = 28
		self.channels = 3
		self.img_shape = (self.img_rows, self.img_cols, self.channels)
		self.latent_dim = latent_dim
		optimizer = Adam(0.0002, 0.5)
		# Build and compile the discriminator
		self.discriminator = self.build_discriminator()
		self.discriminator.compile(loss='binary_crossentropy', optimizer=optimizer, metrics=['accuracy'])
		# Build the encoder / decoder
		self.encoder = self.build_encoder()
		self.decoder = self.build_decoder()
		img = Input(shape=self.img_shape)
		# The generator takes the image, encodes it and reconstructs it
		# from the encoding
		encoded_repr = self.encoder(img)
		reconstructed_img = self.decoder(encoded_repr)
		# For the adversarial_autoencoder model we will only train the generator
		self.discriminator.trainable = False
		# The discriminator determines validity of the encoding
		validity = self.discriminator(encoded_repr)
		# The adversarial_autoencoder model  (stacked generator and discriminator)
		self.adversarial_autoencoder = Model(img, [reconstructed_img, validity])
		self.adversarial_autoencoder.compile(loss=['mse', 'binary_crossentropy'], loss_weights=[0.999, 0.001], optimizer=optimizer)

	def build_encoder(self):
		# Encoder
		img = Input(shape=self.img_shape)
		h = Flatten()(img)
		h = Dense(512)(h)
		h = LeakyReLU(alpha=0.2)(h)
		h = Dense(512)(h)
		h = LeakyReLU(alpha=0.2)(h)
		mu = Dense(self.latent_dim)(h)
		log_var = Dense(self.latent_dim)(h)
		#latent_repr = merge([mu, log_var], mode=lambda p: p[0] + K.random_normal(K.shape(p[0])) * K.exp(p[1] / 2), output_shape=lambda p: p[0])
		latent_repr = Lambda(self.sampling, output_shape=(self.latent_dim,), name='z')([mu, log_var])
		return Model(img, latent_repr)

	def sampling(self, args):
		z_mean, z_log_var = args
		batch = K.shape(z_mean)[0]
		dim = K.int_shape(z_mean)[1]
		# by default, random_normal has mean=0 and std=1.0
		epsilon = K.random_normal(shape=(batch, dim))
		return z_mean + K.exp(0.5 * z_log_var) * epsilon

	def build_decoder(self):
		model = Sequential(name="Decoder")
		model.add(Dense(512, input_dim=self.latent_dim))
		model.add(LeakyReLU(alpha=0.2))
		model.add(Dense(512))
		model.add(LeakyReLU(alpha=0.2))
		model.add(Dense(np.prod(self.img_shape), activation='tanh'))
		model.add(Reshape(self.img_shape))
		model.summary()
		z = Input(shape=(self.latent_dim,))
		img = model(z)
		return Model(z, img)

	def build_discriminator(self):
		model = Sequential(name="Discriminator")
		model.add(Dense(512, input_dim=self.latent_dim))
		model.add(LeakyReLU(alpha=0.2))
		model.add(Dense(256))
		model.add(LeakyReLU(alpha=0.2))
		model.add(Dense(1, activation="sigmoid"))
		model.summary()
		encoded_repr = Input(shape=(self.latent_dim, ))
		validity = model(encoded_repr)
		return Model(encoded_repr, validity)

	def load_MNIST(self):
		(X_train, y_train), (_, _) = mnist.load_data()
		X_train = convertImgsArray(X_train, colorMode="color")
		X_train = (X_train.astype('float32') - 127.5) / 127.5  # Normalize the images to [-1, 1]
		return X_train, y_train

	def load_CIFAR(self):
		# cifar_classes = ["airplane", "automobile", "bird", "cat", "deer", "dog", "frog", "horse", "ship", "truck"]
		(X_train, y_train), (_, _) = cifar10.load_data()
		# Resize the colored images 32x32 to 28x28 and convert them to grayscale.
		X_train = resizeImgsArray(X_train, width=28, height=28, colorMode="color")
		X_train = (X_train.astype('float32') - 127.5) / 127.5  # Normalize the images to [-1, 1]
		return X_train, y_train

	def train(self, epochs, batch_size=128, sample_interval=50):
		self.initialize_directories()
		# Load the dataset
		#X_train = self.load_MNIST()
		X_train, y_train = self.load_CIFAR()
		# Adversarial ground truths
		valid = np.ones((batch_size, 1))
		fake = np.zeros((batch_size, 1))
		for epoch in range(epochs):
			# Train Discriminator
			# Select a random batch of images
			idx = np.random.randint(0, X_train.shape[0], batch_size)
			imgs = X_train[idx]
			latent_fake = self.encoder.predict(imgs)
			latent_real = np.random.normal(size=(batch_size, self.latent_dim))
			# Train the discriminator
			d_loss_real, d_accuracy_real = self.discriminator.train_on_batch(latent_real, valid) # return ['loss', 'accuracy']
			d_loss_fake, d_accuracy_fake = self.discriminator.train_on_batch(latent_fake, fake) # return ['loss', 'model_2_loss', 'model_loss']
			d_loss = 0.5 * np.add(d_loss_real, d_loss_fake)
			d_accuracy = 0.5 * np.add(d_accuracy_real, d_accuracy_fake)
			# Train Generator
			# Train the generator
			g_loss, g_mse_loss, g_bin_crossentropy_loss = self.adversarial_autoencoder.train_on_batch(imgs, [imgs, valid])
			# Plot the progress
			print ("Epoch %d [D loss: %f, D acc: %.2f%%, D acc real %.2f%%, D acc fake %.2f%%] [G loss: %f]" \
				% (epoch+1, d_loss, 100*d_accuracy, 100*d_accuracy_real, 100*d_accuracy_fake, g_loss))
			self.save_results(epoch+1, [d_loss, 100*d_accuracy, 100*d_accuracy_real, 100*d_accuracy_fake, g_loss], sample_interval)
		self.save_decoder()
		# VAE plots if latent_dim=2
		if self.latent_dim==2:
			self.plot_latent()
			self.plot_label_clusters(X_train, y_train)
		#imgsPIL = loadImgsPIL(trainingGeneratedImagesPath)
		#self.generate_gif(imgsPIL, trainingGeneratedImagesPath)
		#print("Metrics D", self.discriminator.metrics_names) # ['loss', 'accuracy']
		#print("Metrics G", self.adversarial_autoencoder.metrics_names) # ['loss', 'model_2_loss', 'model_loss']

	def save_results(self, epoch, resultsList, sample_interval):
		loss_interval = 1
		if epoch % loss_interval == 0:
			with open(resultsPath + "results_" + generativeModelName +"_latent_dim_"+str(self.latent_dim)+".csv", "a", newline='') as csvfile:
				writer = csv.writer(csvfile, delimiter=';')
				writer.writerow([epoch]+resultsList)
		if epoch % sample_interval == 0: # Save generated image samples
			self.sample_images(epoch)

	def sample_images(self, epoch):
		r, c = 5, 5
		z = np.random.normal(size=(r*c, self.latent_dim))
		gen_imgs = self.decoder.predict(z)
		# Rescale images 0 - 1
		gen_imgs = 0.5 * gen_imgs + 0.5
		fig, axs = plt.subplots(r, c)
		cnt = 0
		for i in range(r):
			for j in range(c):
				imgs = gen_imgs[cnt] = (gen_imgs[cnt]) * 127.5 + 127.5
				imgs = imgs.astype(int)
				axs[i,j].imshow(imgs)
				axs[i,j].axis('off')
				cnt += 1
		fig.suptitle("Epoch " + str(epoch))
		path = trainingGeneratedImagesPath+"latent_dim_"+str(self.latent_dim)+"/"
		fig.savefig(path+"%d.png" % epoch)
		plt.close()

	def plot_latent(self):
		# display a n * n 2D manifold of images
		n = 10
		img_dim = 28
		img_channel = 3
		scale = 2.0
		figsize = 15
		figure = np.zeros((img_dim * n, img_dim * n, img_channel))
		# linearly spaced coordinates corresponding to the 2D plot
		# of img classes in the latent space
		grid_x = np.linspace(-scale, scale, n)
		grid_y = np.linspace(-scale, scale, n)[::-1]
		for i, yi in enumerate(grid_y):
			for j, xi in enumerate(grid_x):
				z_sample = np.array([[xi, yi]])
				x_decoded = self.decoder.predict(z_sample)
				x_decoded = x_decoded * 255
				x_decoded = x_decoded.astype(np.uint8)
				img = x_decoded[0].reshape(img_dim, img_dim, img_channel)
				figure[
					i * img_dim : (i + 1) * img_dim,
					j * img_dim : (j + 1) * img_dim,
				] = img
		plt.figure(figsize=(figsize, figsize))
		start_range = img_dim // 2
		end_range = n * img_dim + start_range
		pixel_range = np.arange(start_range, end_range, img_dim)
		sample_range_x = np.round(grid_x, 1)
		sample_range_y = np.round(grid_y, 1)
		plt.xticks(pixel_range, sample_range_x)
		plt.yticks(pixel_range, sample_range_y)
		plt.xlabel("z[0]")
		plt.ylabel("z[1]")
		figure = figure.astype(np.uint8)
		plt.imshow(figure)
		plt.savefig(plotsPath + generativeModelName +'_latent.png', bbox_inches='tight', pad_inches=0)
		#plt.show()

	def plot_label_clusters(self, data, test_lab):
		cifar_classes = ["airplane", "car", "bird", "cat", "deer", "dog", "frog", "horse", "ship", "truck"]
		labels = {i : cifar_classes[i] for i in range(len(cifar_classes))}
		z_mean = self.encoder.predict(data)
		plt.figure(figsize =(12, 10))
		sc = plt.scatter(z_mean[:, 0], z_mean[:, 1], c = test_lab)
		cbar = plt.colorbar(sc, ticks = range(10))
		cbar.ax.set_yticklabels([labels.get(i) for i in range(10)])
		plt.xlabel("z[0]")
		plt.ylabel("z[1]")
		plt.savefig(plotsPath + generativeModelName +'_label_clusters.png', bbox_inches='tight', pad_inches=0)
		#plt.show()

	def generate_imgs(self, imgsNb=10):
		z = np.random.normal(size=(imgsNb, self.latent_dim))
		gen_imgs = self.decoder.predict(z)
		gen_imgs = (gen_imgs) * 127.5 + 127.5
		gen_imgs = gen_imgs.astype(np.uint8)
		gen_imgs = np.squeeze(gen_imgs)
		return gen_imgs

	def generate_imgs_to(self, path, imgsNb=10):
		genImgs = self.generate_imgs(imgsNb)
		genImgsPIL = convertToImgPIL(genImgs)
		emptyDirectory(path)
		createDirectory(path)
		saveImgsPIL(genImgsPIL, path)
		#self.generate_gif(genImgsPIL, path)

	def generate_gif(self, imgsPIL, path):
		generateGif(imgsPIL, path, filename=generativeModelName+"_latent_dim_"+str(self.latent_dim))

	def initialize_directories(self):
		createDirectory(trainingGeneratedImagesPath+"latent_dim_"+str(self.latent_dim)+"/")
		createDirectory(resultsPath)
		removeFile(resultsPath + resultsCsvFilename)
		emptyDirectory(trainingGeneratedImagesPath+"latent_dim_"+str(self.latent_dim)+"/")
		createDirectory(plotsPath)

	def save_decoder(self):
		createDirectory(modelsPath)
		self.decoder.save(modelsPath+generativeModelName+"_latent_dim_"+str(self.latent_dim)+"_decoder.h5")

if __name__ == '__main__':
	latent_dim = 2
	aae = AdversarialAutoencoder(latent_dim)
	f = open("training_time.txt", "a")
	start = time.time()
	epochs = 10000
	batch_size = 32
	aae.train(epochs=epochs, batch_size=batch_size, sample_interval=100)
	end = time.time()
	elapsedTime_secs = end - start
	elapsedTime_hms = convert_secs_to_hms(elapsedTime_secs)
	print("Elapsed time:", elapsedTime_hms)
	f.write(generativeModelName+", Epochs "+str(epochs)+", Batch Size "+str(batch_size)+", Latent Dim "+str(latent_dim)+", Training Time "+elapsedTime_hms+"\n")
	f.close()
	path = "./Generated_Images/"+generativeModelName+"_latent_dim_"+str(latent_dim)+"/"
	aae.generate_imgs_to(path, 100)

	latent_dim = 10
	aae = AdversarialAutoencoder(latent_dim)
	f = open("training_time.txt", "a")
	start = time.time()
	epochs = 10000
	batch_size = 32
	aae.train(epochs=epochs, batch_size=batch_size, sample_interval=100)
	end = time.time()
	elapsedTime_secs = end - start
	elapsedTime_hms = convert_secs_to_hms(elapsedTime_secs)
	print("Elapsed time:", elapsedTime_hms)
	f.write(generativeModelName+", Epochs "+str(epochs)+", Batch Size "+str(batch_size)+", Latent Dim "+str(latent_dim)+", Training Time "+elapsedTime_hms+"\n")
	f.close()
	path = "./Generated_Images/"+generativeModelName+"_latent_dim_"+str(latent_dim)+"/"
	aae.generate_imgs_to(path, 100)