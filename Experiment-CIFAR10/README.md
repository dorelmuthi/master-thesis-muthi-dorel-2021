The python version that we used is python 3.10.4

Install the required libraries with 
pip install -r requirements.txt

or install the following libraries in this order with:

pip install tensorflow==2.8.0

pip install tensorflow-gpu==2.8.0

pip install matplotlib==3.5.2

pip install Pillow==9.1.1

pip install numpy==1.22.4

pip install pandas==1.4.2

pip install seaborn==0.11.2

Execute in the following order the scripts to get the results of the experiment: 

To train the models:

1. gan.py
2. dcgan.py
3. wgan.py
4. aae.py (this will generate two plots related to the latent space)
5. vae.py (this will generate two plots related to the latent space)

To make the results:

6. results.py (to make the different plots in the Plots folder)
7. generateGifs.py (to generate the gifs with the images saved during the training of the models)
8. generateImgs.py (to generate images for the image evaluation metrics)
9. CIFAR-10-classifier.py (to save model for classificationTest.py if not already saved in CIFAR_10_Classifier_Models)
10. classificationTest.py (to make the plot representing the classification test)
12. use the Instructions.txt file of precision-recall-distributions to generate the precision-recall plot.
13. inceptionScore.py (to compute the inception score of the models)
14. fidScore.py (to compute the FID score of the models)



