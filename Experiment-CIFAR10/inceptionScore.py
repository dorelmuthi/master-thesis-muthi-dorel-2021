# Code based on: 
# https://machinelearningmastery.com/how-to-implement-the-inception-score-from-scratch-for-evaluating-generated-images/
# Inception Score (IS)
import numpy as np
import math
from tensorflow.keras.applications.inception_v3 import InceptionV3
from tensorflow.keras.applications.inception_v3 import preprocess_input
import csv
from utils import*

def getInceptionScore(generativeModelName, latent_dim=0, n_split=10, eps=1E-16):
    # Load Generated Images
    if latent_dim==0:
        generatedImagesPath = "./Generated_Images2/"+generativeModelName+"/"
    else:
        generatedImagesPath = "./Generated_Images2/"+generativeModelName+"_latent_dim_"+str(latent_dim)+"/"
    genImgs = loadImgsArray(generatedImagesPath, colorMode="color", width=299, height=299)
    images = genImgs.astype('float32') # convert from uint8 to float32
    # images have the shape 299x299x3, pixels in [0,255]
    model = InceptionV3() # load inception v3 model
    processed = preprocess_input(images) # pre-process raw images for inception v3 model
    yhat = model.predict(processed) # predict class probabilities for images
    scores = list() # enumerate splits of images/predictions
    n_part = math.floor(images.shape[0] / n_split)
    for i in range(n_split):
        # retrieve p(y|x)
        ix_start, ix_end = i * n_part, i * n_part + n_part
        p_yx = yhat[ix_start:ix_end]
        # calculate p(y)
        p_y = np.expand_dims(p_yx.mean(axis=0), 0)
        # calculate KL divergence using log probabilities
        kl_d = p_yx * (np.log(p_yx + eps) - np.log(p_y + eps))
        # sum over classes
        sum_kl_d = kl_d.sum(axis=1)
        # average over images
        avg_kl_d = np.mean(sum_kl_d)
        # undo the log
        is_score = np.exp(avg_kl_d)
        # store
        scores.append(is_score)
    # average across images
    is_avg, is_std = np.mean(scores), np.std(scores)
    return is_avg, is_std

if __name__ == '__main__':
    resultsPath="./Results/"
    createDirectory(resultsPath)
    f = open(resultsPath+'Inception_Score.csv', 'w',  newline='')
    writer = csv.writer(f, delimiter=';')
    writer.writerow(["Generative Model", "IS avg", "IS std"])
    is_avg, is_std = getInceptionScore(generativeModelName="GAN")
    print('GAN IS avg', is_avg, 'IS std', is_std)
    writer.writerow(["GAN", is_avg, is_std])
    is_avg, is_std = getInceptionScore(generativeModelName="DCGAN")
    print('DCGAN IS avg', is_avg, 'IS std', is_std)
    writer.writerow(["DCGAN", is_avg, is_std])
    is_avg, is_std = getInceptionScore(generativeModelName="WGAN")
    print('WGAN IS avg', is_avg, 'IS std', is_std)
    writer.writerow(["WGAN", is_avg, is_std])
    is_avg, is_std = getInceptionScore(generativeModelName="AAE",latent_dim=10)
    print('AAE IS avg', is_avg, 'IS std', is_std)
    writer.writerow(["AAE", is_avg, is_std])
    is_avg, is_std = getInceptionScore(generativeModelName="VAE",latent_dim=10)
    print('VAE IS avg', is_avg, 'IS std', is_std)
    writer.writerow(["VAE", is_avg, is_std])
    f.close()
