import tensorflow as tf
from tensorflow.keras.datasets import cifar10
import numpy as np
import matplotlib.pyplot as plt
from utils import *

def saveRealImages(imgsNb=100):
	(x_train, y_train), (x_test, y_test) = cifar10.load_data()
	np.random.shuffle(x_train)
	realImgs = x_train[:imgsNb]
	realImgs = resizeImgsArray(realImgs, width=28, height=28, colorMode="color")
	realImgsPIL = convertToImgPIL(realImgs)
	realImgsPath = "./Real_Images/"
	createDirectory(realImgsPath)
	saveImgsPIL(realImgsPIL, realImgsPath)
	return realImgs

def loadModel(path):
	model = tf.keras.models.load_model(path)
	return model

def generateImgsFromGAN(generator, imgsNb=10):
	latent_dim = 100
	noise = np.random.normal(0, 1, (imgsNb, latent_dim))
	gen_imgs = generator.predict(noise)
	gen_imgs = (gen_imgs) * 127.5 + 127.5
	gen_imgs = gen_imgs.astype(np.uint8)
	gen_imgs = np.squeeze(gen_imgs)
	return gen_imgs

def generateImgsFromAAE(decoder, imgsNb=10, latent_dim=10):
	z = np.random.normal(size=(imgsNb, latent_dim))
	gen_imgs = decoder.predict(z)
	gen_imgs = (gen_imgs) * 127.5 + 127.5
	gen_imgs = gen_imgs.astype(np.uint8)
	gen_imgs = np.squeeze(gen_imgs)
	return gen_imgs

def generateImgsFromVAE(decoder, imgsNb=10, latent_dim=2):
	gen_imgs = decoder.predict(np.random.normal(size=(imgsNb, latent_dim)))
	gen_imgs = gen_imgs * 255
	gen_imgs = gen_imgs.astype(np.uint8)
	return gen_imgs

def saveImgs(genImgsArray, genModelName):
	path=generatedImagesPath+genModelName+"/"
	genImgsPIL = convertToImgPIL(genImgsArray)
	createDirectory(path)
	saveImgsPIL(genImgsPIL, path)

if __name__ == '__main__':
	plotsPath = "./Plots/"
	createDirectory(plotsPath)
	imgsNb = 1000
	generatedImagesPath = "./Generated_Images2/"
	createDirectory(generatedImagesPath)
	ganGen = loadModel('./Models/GAN_generator.h5')
	genImgsGAN = generateImgsFromGAN(ganGen, imgsNb)
	saveImgs(genImgsGAN, "GAN")
	
	ganGen = loadModel('./Models/DCGAN_generator.h5')
	genImgsDCGAN = generateImgsFromGAN(ganGen, imgsNb)
	saveImgs(genImgsDCGAN, "DCGAN")

	ganGen = loadModel('./Models/WGAN_generator.h5')
	genImgsWGAN = generateImgsFromGAN(ganGen, imgsNb)
	saveImgs(genImgsWGAN, "WGAN")

	aaeDecoder = loadModel('./Models/AAE_latent_dim_2_decoder.h5')
	genImgsAAE_LD_2 = generateImgsFromAAE(aaeDecoder, imgsNb, latent_dim=2)
	saveImgs(genImgsAAE_LD_2, "AAE_latent_dim_2")

	aaeDecoder = loadModel('./Models/AAE_latent_dim_10_decoder.h5')
	genImgsAAE_LD_10 = generateImgsFromAAE(aaeDecoder, imgsNb, latent_dim=10)
	saveImgs(genImgsAAE_LD_10, "AAE_latent_dim_10")

	vaeDecoder = loadModel('./Models/VAE_latent_dim_2_decoder.h5')
	genImgsVAE_LD_2 = generateImgsFromVAE(vaeDecoder, imgsNb, latent_dim=2)
	saveImgs(genImgsVAE_LD_2, "VAE_latent_dim_2")

	vaeDecoder = loadModel('./Models/VAE_latent_dim_10_decoder.h5')
	genImgsVAE_LD_10 = generateImgsFromVAE(vaeDecoder, imgsNb, latent_dim=10)
	saveImgs(genImgsVAE_LD_10, "VAE_latent_dim_10")

	realImgs = saveRealImages(imgsNb)

	# Make a figure with the images generated in order to compare the generative models.
	n = 10
	imgsList = [realImgs[:n], genImgsGAN[:n], genImgsDCGAN[:n], genImgsWGAN[:n], genImgsAAE_LD_2[:n], genImgsAAE_LD_10[:n], genImgsVAE_LD_2[:n], genImgsVAE_LD_10[:n]]
	rows = ["Real", "GAN", "DCGAN", "WGAN", "AAE_LD_2", "AAE_LD_10", "VAE_LD_2", "VAE_LD_10"]
	f, axis = plt.subplots(nrows=8, ncols=n, figsize=(16, 8), sharey=True)
	#plt.subplots_adjust(bottom=None, right=None, top=None, wspace=None, hspace=0.4)
	for i, ax in enumerate(axis):
		for j, col in enumerate(ax):
			#col.axis('off')
			col.set_xticklabels([])
			col.set_yticks([])
			col.set_xticks([])
			col.spines['top'].set_visible(False)
			col.spines['right'].set_visible(False)
			col.spines['bottom'].set_visible(False)
			col.spines['left'].set_visible(False)
			col.imshow(imgsList[i][j])
	for ax, row in zip(axis[:,0], rows):
		ax.set_ylabel(row+"                 ", rotation=0, size='large')
	plt.savefig(plotsPath+'results-comparison.png', bbox_inches='tight', pad_inches=0)