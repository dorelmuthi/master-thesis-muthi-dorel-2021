# Source: https://www.geeksforgeeks.org/cifar-10-image-classification-in-tensorflow/
import tensorflow as tf
print(tf.__version__) # Display the version
import numpy as np
import matplotlib.pyplot as plt
from tensorflow.keras.layers import Input, Conv2D, Dense, Flatten, Dropout
from tensorflow.keras.layers import GlobalMaxPooling2D, MaxPooling2D
from tensorflow.keras.layers import BatchNormalization
from tensorflow.keras.models import Model
from utils import *

cifar10 = tf.keras.datasets.cifar10 # Load in the data
(x_train, y_train), (x_test, y_test) = cifar10.load_data() # Distribute it to train and test set
print(x_train.shape, y_train.shape, x_test.shape, y_test.shape)
x_train, x_test = x_train / 255.0, x_test / 255.0 # Reduce pixel values
y_train, y_test = y_train.flatten(), y_test.flatten() # flatten the label values

K = len(set(y_train)) # number of classes for output layer
print("number of classes:", K)
 
# Build the model using the functional API
i = Input(shape=x_train[0].shape) # Input layer
x = Conv2D(32, (3, 3), activation='relu', padding='same')(i)
x = BatchNormalization()(x)
x = Conv2D(32, (3, 3), activation='relu', padding='same')(x)
x = BatchNormalization()(x)
x = MaxPooling2D((2, 2))(x)
x = Conv2D(64, (3, 3), activation='relu', padding='same')(x)
x = BatchNormalization()(x)
x = Conv2D(64, (3, 3), activation='relu', padding='same')(x)
x = BatchNormalization()(x)
x = MaxPooling2D((2, 2))(x)
x = Conv2D(128, (3, 3), activation='relu', padding='same')(x)
x = BatchNormalization()(x)
x = Conv2D(128, (3, 3), activation='relu', padding='same')(x)
x = BatchNormalization()(x)
x = MaxPooling2D((2, 2))(x)
x = Flatten()(x)
x = Dropout(0.2)(x)
x = Dense(1024, activation='relu')(x) # Hidden layer
x = Dropout(0.2)(x)
x = Dense(K, activation='softmax')(x) # Last hidden layer i.e.. output layer
model = Model(i, x) 
model.summary() # model description

# Compile and Fit
model.compile(optimizer='adam', loss='sparse_categorical_crossentropy', metrics=['accuracy'])
r = model.fit(x_train, y_train, validation_data=(x_test, y_test), epochs=50)

# Evaluate
loss, acc = model.evaluate(x_test, y_test, verbose=2)
print('Model, accuracy: {:5.2f}%'.format(100 * acc))

classifierPath="./CIFAR_10_Classifier_Models/"
createDirectory(classifierPath)
model.save(classifierPath+'CIFAR_10_Classifier.h5')

# Fit with data augmentation
# Note: if you run this AFTER calling the previous model.fit() it will CONTINUE training where it left off
batch_size = 32
data_generator = tf.keras.preprocessing.image.ImageDataGenerator(width_shift_range=0.1, height_shift_range=0.1, horizontal_flip=True)
train_generator = data_generator.flow(x_train, y_train, batch_size)
steps_per_epoch = x_train.shape[0] // batch_size 
r = model.fit(train_generator, validation_data=(x_test, y_test), steps_per_epoch=steps_per_epoch, epochs=50)

# Evaluate
loss, acc = model.evaluate(x_test, y_test, verbose=2)
print('Model, accuracy: {:5.2f}%'.format(100 * acc))

model.save(classifierPath+'CIFAR_10_Classifier2.h5')